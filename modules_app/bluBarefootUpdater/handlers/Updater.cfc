/**
* A normal ColdBox Event Handler
*/
component{
	property name="BarefootService" inject="BarefootService@bluBarefootUpdater";
	property name="UpdaterService" inject="UpdaterService@bluBarefootUpdater";
	property name="settingService" inject="id:settingService@bluBarefootUpdater";
	property name="bluDB" 			inject="DBUpdaterService@bluBarefootUpdater";
	property name = "query" 	inject = "provider:QueryBuilder@qb";
    property name = "hyper" 	inject = "HyperBuilder@Hyper";
    

    function preHandler( event, rc, prc ){
		var barefootSettings = query.from('cb_setting')
			.where('name','blubarefootupdater_settings')
			.get()[1].value;
		prc.barefoot_settings=deserializeJSON( barefootSettings );
		prc.barefoot.username=prc.barefoot_settings.username;
		prc.barefoot.password=prc.barefoot_settings.password;
		prc.barefoot.barefootaccount=prc.barefoot_settings.account;
		prc.barefoot.fieldmappings=prc.barefoot_settings.fieldmappings;
	}

    function postHandler( event, rc, prc ){
		writeOutput( '<h1>Task Completed:'&now()&'</h1>' );
	}

	function index( event, rc, prc ){
		prc.qBarefoot = BarefootService.index( argumentCollection=arguments );
		//writedump(prc.qbarefoot);
		event.setView( 'updater/index' );
	}
function run( event, rc, prc ){
	prc.qBarefootAval = UpdaterService.avail( argumentCollection=arguments );
return 'ok';
}
	function daily( event, rc, prc ){
		prc.qBarefootGAmenities = UpdaterService.globalamenities( argumentCollection=arguments );
		prc.qBarefootProp = UpdaterService.prop( argumentCollection=arguments );
		prc.qBarefootPhotos = UpdaterService.photos( argumentCollection=arguments );
		//prc.qBarefootRates = UpdaterService.rates(argumentCollection=arguments);
		sleep( 5000 );
		prc.qBarefootCleanup = bluDB.cleanup( argumentCollection=arguments );
		sleep( 5000 );
		prc.qBarefootCleanup2 = bluDB.cleanup2( argumentCollection=arguments );
		event.setView( 'updater/daily' );
	}

	function hourly( event, rc, prc ){
		prc.qBarefootAval = UpdaterService.avail( argumentCollection=arguments );
		sleep( 5000 );
		prc.qBarefootCleanup = bluDB.cleanup( argumentCollection=arguments );
		sleep( 5000 );
		prc.qBarefootCleanup2 = bluDB.cleanup2( argumentCollection=arguments );
		event.setView( 'updater/hourly' );
	}

	function prop( event, rc, prc ){
		prc.qBarefootProp = UpdaterService.prop( argumentCollection=arguments );
		qBarefootCleanup = bluDB.cleanup( argumentCollection=arguments );
		sleep( 5000 );
		prc.qBarefootCleanup2 = bluDB.cleanup2( argumentCollection=arguments );
		event.setView( 'updater/prop' );
	}
	function avail( event, rc, prc ){
		prc.qBarefootAval = UpdaterService.avail( argumentCollection=arguments );
		//writedump(prc.qBarefoot);
		event.setView( 'updater/avail' );
	}
	function rates( event, rc, prc ){
		prc.qBarefootRates = UpdaterService.rates( argumentCollection=arguments );
		sleep( 5000 );
		prc.qBarefootCleanupRates = bluDB.cleanup( argumentCollection=arguments );
		//sleep(5000);
		//prc.qBarefootCleanupRates2 = bluDB.cleanupRates2(argumentCollection=arguments);
		event.setView( 'updater/rates' );
	}
	function globalamenities( event, rc, prc ){
		prc.qBarefootGAmenities = UpdaterService.globalamenities( argumentCollection=arguments );
		//writedump(prc.qBarefoot);
		event.setView( 'updater/prop' );
	}
	function photos( event, rc, prc ){
		prc.qBarefootPhotos = UpdaterService.photos( argumentCollection=arguments );
		//writedump(prc.qBarefoot);
		event.setView( 'updater/photos' );
	}
	function owners( event, rc, prc ){
		prc.qBarefootOwners = UpdaterService.owners( argumentCollection=arguments );
		//writedump(prc.qBarefootOwners);
		event.setView( 'updater/owners' );
	}

	function tenants( event, rc, prc ){
		prc.qBarefoottenants = UpdaterService.tenants( argumentCollection=arguments );
		//writedump(prc.qBarefoottenants);
		event.setView( 'updater/tenants' );
	}

	

}