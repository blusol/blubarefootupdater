<cfcomponent output='true' singleton>
	<cfproperty name="htmlHelper" 		inject="HTMLHelper@coldbox">
	<cfproperty name="settingService" 	inject="settingService@bluBarefootUpdater">
	<cfproperty name="bluDB" 			inject="DBUpdaterService@bluBarefootUpdater">
	<cfproperty name="BarefootService" 			inject="BarefootService@bluBarefootUpdater">

<cffunction name="cleanup" output="true" access="public" returntype="any" hint="">
<cfset q=''>
<cfset local.qDBUpdaterCleanup=#bluDB.cleanup()#>
<cfreturn q>
</cffunction>


<!---
********************************************************************************
							PHOTOS FUNCTION
********************************************************************************
--->
<cffunction name="photos" output="true" access="public" returntype="any" hint="">
<blockquote>
	<cfset q=''>
	<h1>START:bluBarefootUpdater/updater/photos</h1>

	<cfset var bluRentUpdater = '#structNew()#'>


	<cfquery name="getProperties" datasource="contentbox">
		select 
			bluerent__properties.propid
		from
			bluerent__properties
	</cfquery>
	<cfset var lc='0'>
	<ol>
	<cfloop  query="getProperties">
	<li>
	<cfset lc=currentrow>
		
		<cfif #structKeyExists( rc, 'debug' )#>
			<h2>#propid#</h2>
		</cfif>
		<cfobject name="getPhotoService" component="BarefootService"> 
		<cfset local.myqBarefootPhotos=#getPhotoService.GetPropertyAllImgsXML( 'propid'=propid, argumentCollection = arguments )#>	
	
		

		<cfif #structKeyExists( local.myqBarefootPhotos.Property, 'PropertyImg' )#>
			<cfset bluRentUpdater['#lc#'].propid='#propid#'>
		<cfset bluRentUpdater['#lc#'].vendorid='#propid#'>
			<cfloop index="local.photoIndex" from="1" to="#arrayLen( local.myqBarefootPhotos.Property.PropertyImg )#">
				<cfset bluRentUpdater['#lc#'].photos['#local.photoIndex#'].whichorder='#local.myqBarefootPhotos.Property.PropertyImg[local.photoIndex].imageNo.xmltext#'>
				<cfset bluRentUpdater['#lc#'].photos['#local.photoIndex#'].phototitle=''>
				<cfset bluRentUpdater['#lc#'].photos['#local.photoIndex#'].type='JPG'>
				<cfset bluRentUpdater['#lc#'].photos['#local.photoIndex#'].photofilename='#local.myqBarefootPhotos.Property.PropertyImg[local.photoIndex].imagepath.xmltext#'>
					<ul>
						<li><b>bluerent__photos.propid 			:</b>#propid#</li>
				 		<li><b>bluerent__photos.whichorder 		:</b>#local.myqBarefootPhotos.Property.PropertyImg[local.photoIndex].imageNo.xmltext#</li>
				 		<li><b>bluerent__photos.filename 		:</b>#local.myqBarefootPhotos.Property.PropertyImg[local.photoIndex].imagepath.xmltext#</li>

					</ul>
			</cfloop>
		</cfif>
		</li>
	</cfloop>
	</ol>
	<cfset local.qDBUpdater=#bluDB.photos( 'bluRentUpdater'=bluRentUpdater )#>
	<h1>END:bluBarefootUpdater/updater/photos</h1>
	</blockquote>
	<cfreturn q>
</cffunction>

















<!---
********************************************************************************
							PROP FUNCTION
********************************************************************************
--->
<cffunction name="prop" output="true" access="public" returntype="any" hint="">
<blockquote>
	<h1>START:bluBarefootUpdater/updater/prop</h1>
	<cfset var bluRentUpdater = '#structNew()#'>
	<cfset q=''>
	<!--- <cfobject name="getGetProperty" component="BarefootService">  --->
	<cfset local.qBarefoot=#BarefootService.GetProperty( argumentCollection = arguments )#>


	<cfset local.mymapping='#arguments.prc.barefoot.fieldmappings#'>

	<!---manually set mapping, add to variables--->
	<!---
	<cfsavecontent variable="local.mymapping">description:bedlist,name:propname,street:address1,street2:address2,city:city,state:state,zip:zip,a6:area,a192:location,a192:view,a238:lat,a239:lng,a53:occupancy,a56:beds,a195:baths,a302:turnday,KeyboardID:slug,PropertyID:propid,PropertyID:vendorid,extdescription:description</cfsavecontent>--->
	
	<cfquery name="getamenities" datasource="contentbox">
		SELECT 
			* 
		FROM  
			bluerent__amenities
	</cfquery>
	<cfif #structKeyExists( rc, 'debug' )#>
	<h2>start:Key Mappings</h2>
	<ul>
	</cfif>
	<cfloop index="local.propIndex" from="1" to="#arrayLen( local.qBarefoot )#">
		<cfloop list="#local.mymapping#" index="local.mappingIndex">
			<cfif #structKeyExists(
				local.qBarefoot[local.propIndex],
				'#listGetAt(
					local.mappingIndex,
					1,
					':'
				)#'
			)# >
				<cfset bluRentUpdater.properties[local.propIndex][#listGetAt(
					local.mappingIndex,
					2,
					':'
				)#]='#evaluate(
					'local.qBarefoot[local.propIndex].' & listGetAt(
						local.mappingIndex,
						1,
						':'
					) & '.xmltext'
				)#'>
				<cfif #structKeyExists( rc, 'debug' )#>
					<li>
					<b>#listgetat(local.mappingIndex,2,":")#</b>
						:
					#evaluate("local.qBarefoot[local.propIndex]." & listgetat(local.mappingIndex,1,":") & ".xmltext")#
					</li>
				</cfif>
			</cfif>
		</cfloop> 
		
	</cfloop>
	<cfif #structKeyExists( rc, 'debug' )#>
		</ul>
		<h2>end:Key Mappings</h2>
	</cfif>
	<cfloop index="local.propForAmenitiesIndex" from="1" to="#arrayLen( local.qBarefoot )#">
		<!---sub routine for amenities--->
		<cfset var lc=0>
		<cfif #structKeyExists( rc, 'debug' )#>
			<h2>start:Amenities</h2>
			<ul>
		</cfif>
		<cfloop query="getamenities">
			<cfif #structKeyExists( local.qBarefoot[local.propForAmenitiesIndex], code )# and #len(
				evaluate( 'local.qBarefoot[local.propForAmenitiesIndex].' & code &'.xmltext' )
			)#>
				<CFIF #evaluate( 'local.qBarefoot[local.propForAmenitiesIndex].' & code &'.xmltext' )# is not 0>
				<!---[#id#:#code#: #evaluate("local.qBarefoot[local.propForAmenitiesIndex]." & code &".xmltext")#],--->
					<CFIF #evaluate( 'local.qBarefoot[local.propForAmenitiesIndex].' & code &'.xmltext' )# is not 'No Pets Allowed'>
						<cfset lc=lc+1>
						<cfset bluRentUpdater.properties[local.propForAmenitiesIndex]['amenities'][lc]['amenity_id']=id>
						<cfset bluRentUpdater.properties[local.propForAmenitiesIndex]['amenities'][lc]['code']=#local.qBarefoot[local.propForAmenitiesIndex][code]['xmlname']#>
						<cfset bluRentUpdater.properties[local.propForAmenitiesIndex]['amenities'][lc]['name']=#evaluate(
							'local.qBarefoot[local.propForAmenitiesIndex].' & code &'.xmltext'
						)#>
						<cfif #structKeyExists( rc, 'debug' )#>
						<li>#lc#:#id#:#local.qBarefoot[local.propForAmenitiesIndex][code]['xmlname']#:#evaluate("local.qBarefoot[local.propForAmenitiesIndex]." & code &".xmltext")#</li>
						</cfif>
					</CFIF>
				</CFIF>
			</cfif>
		</cfloop>
		<cfif #structKeyExists( rc, 'debug' )#>
			</ul>
			<h2>end:Amenities</h2>
		</cfif>
		<!--- finish amenities--->
	</cfloop>
<!---	<cfdump var="#bluRentUpdater#">--->


	<cfset local.qDBUpdater=#bluDB.prop( 'bluRentUpdater'=bluRentUpdater )#>


<cfset local.qDBUpdaterAmenities=#bluDB.amenities( 'bluRentUpdater'=bluRentUpdater )#>


	<cfset local.qLocations=#bluDB.getLocations()#>

	<cfset local.qAreas=#bluDB.getAreas()#>

	<cfset local.qViews=#bluDB.getViews()#>

	<cfset local.qViews=#bluDB.getCommunities()#>
	
	
		<h1>END:bluBarefootUpdater/updater/prop</h1>
		</blockquote>
	<cfreturn q>
</cffunction>











<!---
********************************************************************************
							GOBAL AMENITIES FUNCTION
********************************************************************************
--->
<cffunction name="globalamenities" output="true" access="public" returntype="any" hint="">
<blockquote>
<h1>START:bluBarefootUpdater/updater/globalamenities</h1>
	<cfset q=''>
	<cfobject name="getGlobalAmenities" component="BarefootService"> 
	<cfset local.qBarefoot=#getGlobalAmenities.GetPropertyAmmenityNameXML( argumentCollection = arguments )#>
	<cfloop
        index="gAmenitiesIndex"
        from="1"
        to="#arrayLen( local.qbarefoot.Property.PropertyAmmenity )#"
		step="1">
		<cfset bluRentUpdater['globalamenities'][gAmenitiesIndex].name=#local.qbarefoot.Property.PropertyAmmenity[gAmenitiesIndex].a_value.xmltext#>
		<cfset bluRentUpdater['globalamenities'][gAmenitiesIndex].code=#local.qbarefoot.Property.PropertyAmmenity[gAmenitiesIndex].a_name.xmltext#>
	</cfloop>
	<cfset local.qDBUpdaterGlobalAmenities=#bluDB.globalamenities( 'bluRentUpdater'=bluRentUpdater.globalamenities )#>
	<h1>END:bluBarefootUpdater/updater/globalamenities</h1>
	</blockquote>
	<cfreturn q>
</cffunction>





<!---
********************************************************************************
							OWNERS FUNCTION
********************************************************************************
--->
<cffunction name="owners" output="true" access="public" returntype="any" hint="">
	<cfset q=''>
	<cfobject name="BarefootService" component="BarefootService"> 
	<cfset local.qBarefoot=#BarefootService.GetOwnerInfo( argumentCollection = arguments )#>
	
	<cfloop
        index="gOwnerIndex"
        from="1"
        to="#arrayLen( local.qbarefoot.GetOwnerInfoResult.XmlChildren[2].XmlChildren[1]['Table'] )#"
		step="1">
<cfscript>

bluRentUpdater['owners'][gOwnerIndex].vendorid=local.qbarefoot.GetOwnerInfoResult.XmlChildren[2].XmlChildren[1]['Table'][gOwnerIndex]['venderid'].xmlText;
bluRentUpdater['owners'][gOwnerIndex].firstname=local.qbarefoot.GetOwnerInfoResult.XmlChildren[2].XmlChildren[1]['Table'][gOwnerIndex]['FirstName'].xmlText;
bluRentUpdater['owners'][gOwnerIndex].lastname=local.qbarefoot.GetOwnerInfoResult.XmlChildren[2].XmlChildren[1]['Table'][gOwnerIndex]['LastName'].xmlText;
bluRentUpdater['owners'][gOwnerIndex].address1=local.qbarefoot.GetOwnerInfoResult.XmlChildren[2].XmlChildren[1]['Table'][gOwnerIndex]['Address1'].xmlText;
bluRentUpdater['owners'][gOwnerIndex].address2=local.qbarefoot.GetOwnerInfoResult.XmlChildren[2].XmlChildren[1]['Table'][gOwnerIndex]['Address2'].xmlText;
bluRentUpdater['owners'][gOwnerIndex].city=local.qbarefoot.GetOwnerInfoResult.XmlChildren[2].XmlChildren[1]['Table'][gOwnerIndex]['City'].xmlText;
bluRentUpdater['owners'][gOwnerIndex].state=local.qbarefoot.GetOwnerInfoResult.XmlChildren[2].XmlChildren[1]['Table'][gOwnerIndex]['State'].xmlText;
bluRentUpdater['owners'][gOwnerIndex].zip=local.qbarefoot.GetOwnerInfoResult.XmlChildren[2].XmlChildren[1]['Table'][gOwnerIndex]['Zip'].xmlText;
bluRentUpdater['owners'][gOwnerIndex].homephone=local.qbarefoot.GetOwnerInfoResult.XmlChildren[2].XmlChildren[1]['Table'][gOwnerIndex]['HomePhone'].xmlText;
bluRentUpdater['owners'][gOwnerIndex].workphone=local.qbarefoot.GetOwnerInfoResult.XmlChildren[2].XmlChildren[1]['Table'][gOwnerIndex]['WorkPhone'].xmlText;
bluRentUpdater['owners'][gOwnerIndex].cellphone=local.qbarefoot.GetOwnerInfoResult.XmlChildren[2].XmlChildren[1]['Table'][gOwnerIndex]['CellPhone'].xmlText;
bluRentUpdater['owners'][gOwnerIndex].email=local.qbarefoot.GetOwnerInfoResult.XmlChildren[2].XmlChildren[1]['Table'][gOwnerIndex]['EMail'].xmlText;
bluRentUpdater['owners'][gOwnerIndex].company=local.qbarefoot.GetOwnerInfoResult.XmlChildren[2].XmlChildren[1]['Table'][gOwnerIndex]['Company'].xmlText;
bluRentUpdater['owners'][gOwnerIndex].vendorownerid=local.qbarefoot.GetOwnerInfoResult.XmlChildren[2].XmlChildren[1]['Table'][gOwnerIndex]['OwnerID'].xmlText;
bluRentUpdater['owners'][gOwnerIndex].vendorgid=local.qbarefoot.GetOwnerInfoResult.XmlChildren[2].XmlChildren[1]['Table'][gOwnerIndex]['GID'].xmlText;
</cfscript>
		</cfloop>
		<cfset local.qDBUpdaterowners=#bluDB.owners( 'bluRentUpdater'=bluRentUpdater.owners )#>
	<cfreturn local.qDBUpdaterowners>
</cffunction>



<!---
********************************************************************************
							tenants FUNCTION
********************************************************************************
--->
<cffunction name="tenants" output="true" access="public" returntype="any" hint="">
<cfsetting requesttimeout="720" />
	<cfset q=''>
	<cfobject name="BarefootService" component="BarefootService"> 
	<cfset local.qBarefoot=#BarefootService.GettenantInfo( argumentCollection = arguments )#>
	
	<cfloop
        index="gtenantIndex"
        from="1"
        to="#arrayLen( local.qbarefoot.GettenantInfoResult.XmlChildren[2].XmlChildren[1]['Table'] )#"
		step="1">
<cfscript>

bluRentUpdater['tenants'][gtenantIndex].vendorid=local.qbarefoot.GettenantInfoResult.XmlChildren[2].XmlChildren[1]['Table'][gtenantIndex]['venderid'].xmlText;
bluRentUpdater['tenants'][gtenantIndex].firstname=local.qbarefoot.GettenantInfoResult.XmlChildren[2].XmlChildren[1]['Table'][gtenantIndex]['FirstName'].xmlText;
bluRentUpdater['tenants'][gtenantIndex].lastname=local.qbarefoot.GettenantInfoResult.XmlChildren[2].XmlChildren[1]['Table'][gtenantIndex]['LastName'].xmlText;
bluRentUpdater['tenants'][gtenantIndex].address1=local.qbarefoot.GettenantInfoResult.XmlChildren[2].XmlChildren[1]['Table'][gtenantIndex]['Address1'].xmlText;
bluRentUpdater['tenants'][gtenantIndex].address2=local.qbarefoot.GettenantInfoResult.XmlChildren[2].XmlChildren[1]['Table'][gtenantIndex]['Address2'].xmlText;
bluRentUpdater['tenants'][gtenantIndex].city=local.qbarefoot.GettenantInfoResult.XmlChildren[2].XmlChildren[1]['Table'][gtenantIndex]['City'].xmlText;
bluRentUpdater['tenants'][gtenantIndex].state=local.qbarefoot.GettenantInfoResult.XmlChildren[2].XmlChildren[1]['Table'][gtenantIndex]['State'].xmlText;
bluRentUpdater['tenants'][gtenantIndex].zip=local.qbarefoot.GettenantInfoResult.XmlChildren[2].XmlChildren[1]['Table'][gtenantIndex]['Zip'].xmlText;
bluRentUpdater['tenants'][gtenantIndex].homephone=local.qbarefoot.GettenantInfoResult.XmlChildren[2].XmlChildren[1]['Table'][gtenantIndex]['HomePhone'].xmlText;
bluRentUpdater['tenants'][gtenantIndex].workphone=local.qbarefoot.GettenantInfoResult.XmlChildren[2].XmlChildren[1]['Table'][gtenantIndex]['WorkPhone'].xmlText;
bluRentUpdater['tenants'][gtenantIndex].cellphone=local.qbarefoot.GettenantInfoResult.XmlChildren[2].XmlChildren[1]['Table'][gtenantIndex]['CellPhone'].xmlText;
bluRentUpdater['tenants'][gtenantIndex].email=local.qbarefoot.GettenantInfoResult.XmlChildren[2].XmlChildren[1]['Table'][gtenantIndex]['EMail'].xmlText;
bluRentUpdater['tenants'][gtenantIndex].company=local.qbarefoot.GettenantInfoResult.XmlChildren[2].XmlChildren[1]['Table'][gtenantIndex]['Company'].xmlText;
bluRentUpdater['tenants'][gtenantIndex].vendorcontactid=local.qbarefoot.GettenantInfoResult.XmlChildren[2].XmlChildren[1]['Table'][gtenantIndex]['contactID'].xmlText;
bluRentUpdater['tenants'][gtenantIndex].vendorgid=local.qbarefoot.GettenantInfoResult.XmlChildren[2].XmlChildren[1]['Table'][gtenantIndex]['GID'].xmlText;
</cfscript>

		</cfloop>
		<cfset local.qDBUpdatertenants=#bluDB.tenants( 'bluRentUpdater'=bluRentUpdater.tenants )#>
	<cfreturn local.qDBUpdatertenants>
</cffunction>




<!---
********************************************************************************
							AVAILABILITY FUNCTION
********************************************************************************
--->
<cffunction name="avail" output="true" access="public" returntype="any" hint="">
<blockquote>
	<cfset q=''>
	<h1>START:bluBarefootUpdater/updater/avail</h1>
	<cfquery name="checkprop" datasource="contentbox">
		select bluerent__properties.propid
		from
			bluerent__properties
	</cfquery>
	<cfset checkproplist=#valueList( checkprop.propid )#>
	<cfobject name="getAvail" component="BarefootService"> 
	<cfset local.qBarefoot=#xmlParse( getAvail.GetPropertyBookingDateForAllXML( argumentCollection = arguments ) )#>
	<cfset var lc=0>
			<cfloop index="i" from="1" to="#arrayLen( local.qBarefoot.XmlRoot.XmlChildren )#">
			<cfif #listFind( checkproplist, '#local.qBarefoot.XmlRoot.XmlChildren[i].XmlChildren[1].xmltext#' )# GT 0>
			<cfset lc=lc+1>
				<cfset bluRentUpdater['avail'][lc]['propid']='#local.qBarefoot.XmlRoot.XmlChildren[i].XmlChildren[1].xmltext#'>
				<cfset bluRentUpdater['avail'][lc]['arrive']='#local.qBarefoot.XmlRoot.XmlChildren[i].XmlChildren[2].xmltext#'>
				<cfset bluRentUpdater['avail'][lc]['depart']='#local.qBarefoot.XmlRoot.XmlChildren[i].XmlChildren[3].xmltext#'>
				<cfset bluRentUpdater['avail'][lc]['vendorstatus']='Confirmed'>
				
			</cfif>
			</cfloop>
	
	<cfset local.qDBUpdaterAvail=#bluDB.avail( 'bluRentUpdater'=bluRentUpdater.avail )#>

	<h1>END:bluBarefootUpdater/updater/avail</h1>
	</blockquote>
	<cfreturn q>
</cffunction>










<!---
********************************************************************************
							RATES FUNCTION (dev)
********************************************************************************
--->
<cffunction name="rates" output="true" access="public" returntype="any" hint="">
	<cfset q=''>

	<cfquery name="getproperties" datasource="contentbox">
		SELECT 
			distinct bluerent__properties.propid
		FROM  
			bluerent__properties
			
			
	</cfquery>
	<cfset lc=0>
<cfloop query="getproperties">
	<cfobject name="getRates" component="BarefootService"> 
	<cfset local.qBarefoot=#xmlParse( getRates.GetPropertyRatesXML( 'propid'=getproperties.propid, argumentCollection = arguments ) )#>
	
<cfset thiskey=''>
	<cfloop index="i" from="1" to="#arrayLen( local.qBarefoot.XmlRoot.XmlChildren )#">
	<cfif #thiskey# is '#getproperties.propid##dateFormat( local.qBarefoot.XmlRoot.XmlChildren[i].XmlChildren[1].xmltext, 'yyyymmdd' )##dateFormat(
		local.qBarefoot.XmlRoot.XmlChildren[i].XmlChildren[2].xmltext,
		'yyyymmdd'
	)#'>
		<cfelse>
		<cfset lc=lc+1>
		</cfif>
	<cfset thiskey='#getproperties.propid##dateFormat( local.qBarefoot.XmlRoot.XmlChildren[i].XmlChildren[1].xmltext, 'yyyymmdd' )##dateFormat(
		local.qBarefoot.XmlRoot.XmlChildren[i].XmlChildren[2].xmltext,
		'yyyymmdd'
	)#'>

		<cfset bluRentUpdater['rates'][lc]['groupkey']	=	'#thiskey#'>
		<cfset bluRentUpdater['rates'][lc]['propid']='#getproperties.propid#'>
		<cfset bluRentUpdater['rates'][lc]['start']='#local.qBarefoot.XmlRoot.XmlChildren[i].XmlChildren[1].xmltext#'>
		<cfset bluRentUpdater['rates'][lc]['end']='#local.qBarefoot.XmlRoot.XmlChildren[i].XmlChildren[2].xmltext#'>
		<cfset bluRentUpdater['rates'][lc]['season']='#local.qBarefoot.XmlRoot.XmlChildren[i].XmlChildren[1].xmltext#-#local.qBarefoot.XmlRoot.XmlChildren[i].XmlChildren[2].xmltext#'>
		<!---<cfset bluRentUpdater['rates'][thiskey]['rent']="#local.qBarefoot.XmlRoot.XmlChildren[i].XmlChildren[3].xmltext#">--->
		<!---<cfset bluRentUpdater['rates'][thiskey]['pricetype']="#local.qBarefoot.XmlRoot.XmlChildren[i].XmlChildren[4].xmltext#">--->
		
		<cfset bluRentUpdater['rates'][lc]['#local.qBarefoot.XmlRoot.XmlChildren[i].XmlChildren[4].xmltext#']='#local.qBarefoot.XmlRoot.XmlChildren[i].XmlChildren[3].xmltext#'>
		<!---<cfswitch expression="#local.qBarefoot.XmlRoot.XmlChildren[i].XmlChildren[4].xmltext#">
			<cfcase value="weekly">
				<cfset bluRentUpdater['rates'][lc]['weekly']="#local.qBarefoot.XmlRoot.XmlChildren[i].XmlChildren[3].xmltext#">
			</cfcase>
			<cfcase value="daily">
				<cfset bluRentUpdater['rates'][lc]['daily']="#local.qBarefoot.XmlRoot.XmlChildren[i].XmlChildren[3].xmltext#">
			</cfcase>
			<cfcase value="monthly">
				<cfset bluRentUpdater['rates'][lc]['monthly']="#local.qBarefoot.XmlRoot.XmlChildren[i].XmlChildren[3].xmltext#">
			</cfcase>


		</cfswitch>--->


		<!---#thiskey#
		|
		#getproperties.propid#
		|
		#local.qBarefoot.XmlRoot.XmlChildren[i].XmlChildren[1].xmltext#
		|
		#local.qBarefoot.XmlRoot.XmlChildren[i].XmlChildren[2].xmltext#
		|
		#local.qBarefoot.XmlRoot.XmlChildren[i].XmlChildren[3].xmltext#
		|
		#local.qBarefoot.XmlRoot.XmlChildren[i].XmlChildren[4].xmltext#
		<br>
--->
	</cfloop>


	
	
	</cfloop>

	<cfset local.qDBUpdaterSeasonsFromRates=#bluDB.seasonsFromRates( 'bluRentUpdater'=bluRentUpdater['rates'] )#>
<!---
<cfdump var="#local.qDBUpdaterSeasonsFromRates#">
--->
<!---
	* add season to rates
--->
<cfloop index="myRateIDIndex" from="1" to="#arrayLen( bluRentUpdater['rates'] )#">
	<cfquery name="rateTest" dbtype="query">
		select * from local.qDBUpdaterSeasonsFromRates 
		where 
			season='#bluRentUpdater['rates'][myRateIDIndex]['season']#'
	</cfquery>
	<cfif #rateTest.recordcount#>
		<cfset bluRentUpdater['rates'][myRateIDIndex]['seasonID']	=	'#rateTest.id#'>
	</cfif>
</cfloop>

	<cfset local.qDBUpdaterRates=#bluDB.rates( 'bluRentUpdater'=bluRentUpdater['rates'] )#>
	
	<cfreturn q>
</cffunction>

			










</cfcomponent>