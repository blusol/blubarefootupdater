<cfcomponent output='true' singleton>
	<!--- Dependencies --->
	<cfproperty name="htmlHelper" inject="HTMLHelper@coldbox">
	<cfproperty name="settingService" inject="settingService@bluBarefootUpdater">
	<cfproperty name = "query" 	inject = "provider:QueryBuilder@qb">

<cffunction name="cleanup" output="true" access="public" returntype="any" hint="">
	<cfset var q=''>
	<blockquote>
	<h3>start:bluRent/DBUpdaterService/cleanup</h3>

		<cfquery name="enableProcessedPhotos" datasource="contentbox" result="enableProcessedPhotosOutput">
			UPDATE bluerent__photos
			SET
				active = 1
			WHERE
				processed = 1
		</cfquery>
		<cfdump var="#enableProcessedPhotosOutput#">
		

		<cfquery name="enableProcessedAvails" datasource="contentbox" result="enableProcessedAvailsOutput">
			UPDATE bluerent__avails
			SET
				active = 1
			WHERE
				processed = 1
		</cfquery>
		<cfdump var="#enableProcessedAvailsOutput#">


		<cfquery name="enableProcessedproperties" datasource="contentbox" result="enableProcessedpropertiesOutput">
			UPDATE bluerent__properties
			SET
				active = 1
			WHERE
				processed = 1
		</cfquery>
		<cfdump var="#enableProcessedpropertiesOutput#">


		<cfquery name="enableProcessedproperties_amenities" datasource="contentbox" result="enableProcessedproperties_amenitiesOutput">
			UPDATE bluerent__properties_amenities
			SET
				active = 1
			WHERE
				processed = 1
		</cfquery>
		<cfdump var="#enableProcessedproperties_amenitiesOutput#">


		



		
		<h3>end:bluRent/DBUpdaterService/cleanup</h3>
		</blockquote>
		<cfreturn q>
</cffunction>

<cffunction name="cleanup2" output="true" access="public" returntype="any" hint="">
	<cfset var q=''>
	<blockquote>
	<h3>start:bluRent/DBUpdaterService/cleanup2</h3>


		<cfquery name="disableUntouchedproperties_amenities" datasource="contentbox" result="disableUntouchedproperties_amenitiesOutput">
			UPDATE bluerent__properties_amenities
			SET
				bluerent__properties_amenities.active = 0
			WHERE
				bluerent__properties_amenities.processed = 0
			AND 
				bluerent__properties_amenities.active = 1
		</cfquery>
		<cfdump var="#disableUntouchedproperties_amenitiesOutput#">
		<cfquery name="disableUntouchedproperties" datasource="contentbox" result="disableUntouchedpropertiesOutput">
			UPDATE bluerent__properties
			SET
				bluerent__properties.active = 0
			WHERE
				bluerent__properties.processed = 0
			AND 
				bluerent__properties.active = 1
		</cfquery>
		<cfdump var="#disableUntouchedpropertiesOutput#">
		<cfquery name="disableUntouchedAvails" datasource="contentbox" result="disableUntouchedAvailsOutput">
			UPDATE bluerent__avails
			SET
				bluerent__avails.active = 0
			WHERE
				bluerent__avails.processed = 0
			AND 
				bluerent__avails.active = 1
		</cfquery>
		<cfdump var="#disableUntouchedAvailsOutput#">
		<cfquery name="disableUntouchedPhotos" datasource="contentbox" result="disableUntouchedPhotosOutput">
			UPDATE bluerent__photos
			SET
				bluerent__photos.active = 0
			WHERE
				bluerent__photos.processed = 0
			AND 
				bluerent__photos.active = 1
		</cfquery>
		<cfdump var="#disableUntouchedPhotosOutput#">
		<h3>end:bluRent/DBUpdaterService/cleanup2</h3>
		</blockquote>
		<cfreturn q>
</cffunction>




<cffunction name="cleanupRates" output="true" access="public" returntype="any" hint="">
	<cfset var q=''>
	<blockquote>
	<h3>start:bluRent/DBUpdaterService/cleanupRates</h3>

		<cfquery name="enableProcessedseasons" datasource="contentbox" result="enableProcessedseasonsOutput">
			UPDATE bluerent__seasons
			SET
				active = 1
			WHERE
				processed = 1
		</cfquery>
		<cfdump var="#enableProcessedseasonsOutput#">


		<cfquery name="enableProcessedrates" datasource="contentbox" result="enableProcessedratesOutput">
			UPDATE bluerent__rates
			SET
				active = 1
			WHERE
				processed = 1
		</cfquery>
		<cfdump var="#enableProcessedratesOutput#">

		<h3>end:bluRent/DBUpdaterService/cleanupRates</h3>
		</blockquote>
		<cfreturn q>
</cffunction>


<cffunction name="cleanupRates2" output="true" access="public" returntype="any" hint="">
	<cfset var q=''>
	<blockquote>
	<h3>start:bluRent/DBUpdaterService/cleanupRates2</h3>
		<cfquery name="disableUntouchedrates" datasource="contentbox" result="disableUntouchedratesOutput">
			UPDATE bluerent__rates
			SET
				bluerent__rates.active = 0
			WHERE
				bluerent__rates.processed = 0
			AND 
				bluerent__rates.active = 1
		</cfquery>
		<cfdump var="#disableUntouchedratesOutput#">
		<cfquery name="disableUntouchedseasons" datasource="contentbox" result="disableUntouchedseasonsOutput">
			UPDATE bluerent__seasons
			SET
				bluerent__seasons.active = 0
			WHERE
				bluerent__seasons.processed = 0
			AND 
				bluerent__seasons.active = 1
		</cfquery>
		<cfdump var="#disableUntouchedseasonsOutput#">
		<h3>end:bluRent/DBUpdaterService/cleanupRates2</h3>
		</blockquote>
		<cfreturn q>
</cffunction>



	<cffunction name="photos" output="true" access="public" returntype="any" hint="">
	<blockquote>
	<h3>start:bluRent/DBUpdaterService/photos</h3>
	<cfset var q=''>
	<cfset local.qPhotos = arguments.bluRentUpdater>
	<cfloop index="local.i" from="1" to="#arrayLen( local.qPhotos )#">
		<cftry>
		<cfquery name="check" datasource="contentbox">
			UPDATE
				bluerent__photos
			set
				bluerent__photos.processed 	= 	0
			where 
				bluerent__photos.propid 	=	<cfqueryparam value="#local.qPhotos[local.i].propid#">
		</cfquery>
		
		<cfloop index="local.ii" from="1" to="#arrayLen( local.qPhotos[local.i].photos )#">
			<cfquery name="check" datasource="contentbox">
				select 
					bluerent__photos.id 
				from 
					bluerent__photos
				where 
					bluerent__photos.propid 		=	<cfqueryparam value="#local.qPhotos[local.i].propid#">
				and
					bluerent__photos.photofilename	=	<cfqueryparam value="#local.qPhotos[local.i].photos[local.ii].photofilename#">
			</cfquery>
		
			<cfquery name="addphotos" datasource="contentbox">
				<cfif #check.recordcount# is 0>
				insert into
					<cfelse>
				update
				</cfif>
					bluerent__photos
		 		set
			 		bluerent__photos.propid 		=	<cfqueryparam value="#local.qPhotos[local.i].propid#">,
			 		bluerent__photos.vendorid 		=	<cfqueryparam value="#local.qPhotos[local.i].vendorid#">,
			 		bluerent__photos.phototitle		= 	<cfqueryparam value="#local.qPhotos[local.i].photos[local.ii].phototitle#">,
			 		bluerent__photos.whichorder 	=	<cfqueryparam value="#local.qPhotos[local.i].photos[local.ii].whichorder#">,
			 		bluerent__photos.photofilename 	=	<cfqueryparam value="#local.qPhotos[local.i].photos[local.ii].photofilename#">,
			 		bluerent__photos.filename 		=	<cfqueryparam value="#local.qPhotos[local.i].photos[local.ii].photofilename#">,
			 		bluerent__photos.type 			=	<cfqueryparam value="#local.qPhotos[local.i].photos[local.ii].type#">,
			 		bluerent__photos.processed 		=	1,
			 		bluerent__photos.external 		=	1,
			 		bluerent__photos.category 		=	'',
			 		bluerent__photos.updated_at 	=	#createODBCDateTime( now() )#
			 		<cfif #check.recordcount# is 0>
			 		,bluerent__photos.created_at 	=	#createODBCDateTime( now() )#
			 		<cfelse>
		 		where 
		 			bluerent__photos.id = <cfqueryparam value="#check.id#">
		 			</cfif>
			</cfquery>


			<ul>
					<li><b>bluerent__photos.propid 			:</b>#local.qPhotos[local.i].propid#</li>
			 		<li><b>bluerent__photos.vendorid 		:</b>#local.qPhotos[local.i].vendorid#</li>
			 		<li><b>bluerent__photos.phototitle		:</b>#local.qPhotos[local.i].photos[local.ii].phototitle#</li>
			 		<li><b>bluerent__photos.whichorder 		:</b>#local.qPhotos[local.i].photos[local.ii].whichorder#</li>
			 		<li><b>bluerent__photos.photofilename 	:</b>#local.qPhotos[local.i].photos[local.ii].photofilename#</li>
			 		<li><b>bluerent__photos.filename 		:</b>#local.qPhotos[local.i].photos[local.ii].photofilename#</li>
			 		<li><b>bluerent__photos.type 			:</b>#local.qPhotos[local.i].photos[local.ii].type#</li>
			</ul>
		
		</cfloop>
		<cfcatch>ERROR<br></cfcatch>
		</cftry>
		
		
	</cfloop>
		<h3>end:bluRent/DBUpdaterService/photos</h3>
		</blockquote>
		<cfreturn q>
	</cffunction>

	<cffunction name="prop" output="true" access="public" returntype="any" hint="">
	<blockquote>
	<h3>start:bluRent/DBUpdaterService/prop</h3>
	<cfset var q='#arguments.bluRentUpdater.properties#'>
	<cfif not #structKeyExists( arguments, 'noflush' )#>
	<cfquery name="flush" datasource="contentbox" result="output">
		UPDATE
			bluerent__properties
		set
			bluerent__properties.processed 	= 	0
	</cfquery>
		<!---<cfdump var="#output#">--->
	</cfif>
	<ol>
	<cfloop index="i" from="1" to="#arrayLen( q )#">
	<cftry>
		<cfquery name="check" datasource="contentbox">
			select 
				bluerent__properties.id 
			from 
				bluerent__properties  
			where 
				bluerent__properties.propid=<cfqueryparam value="#q[i].propid#">
		</cfquery>
		<li>


			<cfquery name="u" datasource="contentbox" result="UpdateOutput">
				<cfif #check.recordcount# is 0>
				<!---********************* [ADD] **********************--->
					INSERT INTO
				<cfelse>
				<!---********************* [UPDATE] **********************--->
					UPDATE  							
				</cfif>
					bluerent__properties  
				SET
					<cfif #structKeyExists( q[i], 'propname' )#>
						bluerent__properties.propname 	= <cfqueryparam value="#q[i].propname#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'propid' )#>
						bluerent__properties.propid 	= <cfqueryparam value="#q[i].propid#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'vendorid' )#>
						bluerent__properties.vendorid	= <cfqueryparam value="#q[i].vendorid#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'slug' )#>
						bluerent__properties.slug		= <cfqueryparam value="#lCase( q[i].slug )#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'address1' )#>
						bluerent__properties.address1	= <cfqueryparam value="#q[i].address1#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'address2' )#>
						bluerent__properties.address2	= <cfqueryparam value="#q[i].address2#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'city' )#>
						bluerent__properties.city		= <cfqueryparam value="#q[i].city#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'state' )#>
						bluerent__properties.state		= <cfqueryparam value="#q[i].state#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'zip' )#>
						bluerent__properties.zip		= <cfqueryparam value="#q[i].zip#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'beds' )#>
						bluerent__properties.beds		= <cfqueryparam value="#q[i].beds#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'baths' )#>
						bluerent__properties.baths		= <cfqueryparam value="#q[i].baths#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'occupancy' )#>
						bluerent__properties.occupancy	= <cfqueryparam value="#q[i].occupancy#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'bedlist' )#>
						bluerent__properties.bedlist	= <cfqueryparam value="#q[i].bedlist#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'summary' )#>
						bluerent__properties.summary	= <cfqueryparam value="#q[i].summary#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'description' )#>
						bluerent__properties.description= <cfqueryparam value="#q[i].description#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'headline' )#>
						bluerent__properties.headline 	= <cfqueryparam value="#q[i].headline#">,
					</cfif>	
					<cfif #structKeyExists( q[i], 'lat' )#>
						bluerent__properties.lat 		= <cfqueryparam value="#q[i].lat#">,
					</cfif>	
					<cfif #structKeyExists( q[i], 'lng' )#>
						bluerent__properties.lng 		= <cfqueryparam value="#q[i].lng#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'area' )#>
						bluerent__properties.area  		= <cfqueryparam value="#q[i].area#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'location' )#>
						bluerent__properties.location  	= <cfqueryparam value="#q[i].location#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'community' )#>
						bluerent__properties.community  	= <cfqueryparam value="#q[i].community#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'view' )#>
						bluerent__properties.view 		= <cfqueryparam value="#q[i].view#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'turnday' )#>
						bluerent__properties.turnday 	= <cfqueryparam value="#q[i].turnday#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'type' )#>
						bluerent__properties.type 		= <cfqueryparam value="#q[i].type#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'phone' )#>
						bluerent__properties.phone 		= <cfqueryparam value="#q[i].phone#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'attributes' )#>
						bluerent__properties.attributes 		= <cfqueryparam value="#q[i].attributes#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'petfriendly' )#>
						bluerent__properties.petfriendly= <cfqueryparam value="#q[i].petfriendly#">,
					</cfif>
				<cfif #check.recordcount# eq 0>
					bluerent__properties.created_at 	= 	#createODBCDateTime( now() )#,
				</cfif>
					bluerent__properties.updated_at 	= 	#createODBCDateTime( now() )#,
					bluerent__properties.processed		=	1
				<cfif #check.recordcount# gt 0>
					where
						bluerent__properties.id 			= 	<cfqueryparam value="#check.id#">
				</cfif>
			</cfquery>	
			<!---<cfdump var="#UpdateOutput#">--->

<ul>
<cfif #structKeyExists( q[i], 'propname' )#>
	<li><b>bluerent__properties.propname:</b>#q[i].propname#</li>
</cfif>
<cfif #structKeyExists( q[i], 'propid' )#>
	<li><b>bluerent__properties.propid:</b>#q[i].propid#</li>
</cfif>
<cfif #structKeyExists( q[i], 'vendorid' )#>
	<li><b>bluerent__properties.vendorid	:</b> #q[i].vendorid#</li>
</cfif>
<cfif #structKeyExists( q[i], 'slug' )#>
	<li><b>bluerent__properties.slug		:</b> #lcase(q[i].slug)#</li>
</cfif>
<cfif #structKeyExists( q[i], 'address1' )#>
	<li><b>bluerent__properties.address1	:</b> #q[i].address1#</li>
</cfif>
<cfif #structKeyExists( q[i], 'address2' )#>
	<li><b>bluerent__properties.address2	:</b> #q[i].address2#</li>
</cfif>
<cfif #structKeyExists( q[i], 'city' )#>
	<li><b>bluerent__properties.city		:</b> #q[i].city#</li>
</cfif>
<cfif #structKeyExists( q[i], 'state' )#>
	<li><b>bluerent__properties.state		:</b> #q[i].state#</li>
</cfif>
<cfif #structKeyExists( q[i], 'zip' )#>
	<li><b>bluerent__properties.zip		:</b> #q[i].zip#</li>
</cfif>
<cfif #structKeyExists( q[i], 'beds' )#>
	<li><b>bluerent__properties.beds		:</b> #q[i].beds#</li>
</cfif>
<cfif #structKeyExists( q[i], 'baths' )#>
	<li><b>bluerent__properties.baths		:</b> #q[i].baths#</li>
</cfif>
<cfif #structKeyExists( q[i], 'occupancy' )#>
	<li><b>bluerent__properties.occupancy	:</b> #q[i].occupancy#</li>
</cfif>
<cfif #structKeyExists( q[i], 'bedlist' )#>
	<li><b>bluerent__properties.bedlist	:</b> #q[i].bedlist#</li>
</cfif>
<cfif #structKeyExists( q[i], 'summary' )#>
	<li><b>bluerent__properties.summary	:</b> #q[i].summary#</li>
</cfif>
<cfif #structKeyExists( q[i], 'description' )#>
	<li><b>bluerent__properties.description:</b> #len(q[i].description)# Characters</li>
</cfif>
<cfif #structKeyExists( q[i], 'headline' )#>
	<li><b>bluerent__properties.headline:</b>#q[i].headline#</li>
</cfif>	
<cfif #structKeyExists( q[i], 'lat' )#>
	<li><b>bluerent__properties.lat:</b>#q[i].lat#</li>
</cfif>	
<cfif #structKeyExists( q[i], 'lng' )#>
	<li><b>bluerent__properties.lng:</b>#q[i].lng#</li>
</cfif>
<cfif #structKeyExists( q[i], 'area' )#>
	<li><b>bluerent__properties.area :</b>#q[i].area#</li>
</cfif>
<cfif #structKeyExists( q[i], 'location' )#>
	<li><b>bluerent__properties.location :</b>#q[i].location#</li>
</cfif>
<cfif #structKeyExists( q[i], 'community' )#>
	<li><b>bluerent__properties.community :</b>#q[i].community#</li>
</cfif>
<cfif #structKeyExists( q[i], 'view' )#>
	<li><b>bluerent__properties.view:</b>#q[i].view#</li>
</cfif>
<cfif #structKeyExists( q[i], 'turnday' )#>
	<li><b>bluerent__properties.turnday:</b>#q[i].turnday#</li>
</cfif>
<cfif #structKeyExists( q[i], 'type' )#>
	<li><b>bluerent__properties.type:</b>#q[i].type#</li>
</cfif>
<cfif #structKeyExists( q[i], 'phone' )#>
	<li><b>bluerent__properties.phone:</b>#q[i].phone#</li>
</cfif>
<cfif #structKeyExists( q[i], 'petfriendly' )#>
	<li><b>bluerent__properties.petfriendly :</b> #q[i].petfriendly#</li>
</cfif>
</ul>
</li>
			 
		
		<cfcatch><h4>ERROR:#cfcatch.message# #cfcatch.detail#</h4></cfcatch>
		</cftry>
	</cfloop>
	</ol>

	<cfscript>
		// Pull the existing settings

		var bluRent_settings = query.from('cb_setting')
			.where('name','bluRent_settings')
			.get()[1].value;
		var dSettings['bluRent_settings'] = deserializeJSON( bluRent_settings );

		// Set the locations, areas and views to the method call results --->
		dSettings['bluRent_settings'].locations = getLocations();
		dSettings['bluRent_settings'].views = getViews();
		dSettings['bluRent_settings'].areas = getAreas();
		dSettings['bluRent_settings'].types = getTypes();

		for( key in dSettings ){
			modulesettings[key]=serializeJSON( dSettings[key] );
		}
		writeDump( modulesettings );
		// Save it to settings with SettingsService --->
		//SettingService.bulkSave( modulesettings );
	</cfscript>

	
	
	<h3>end:bluRent/DBUpdaterService/prop</h3>
	</blockquote>
	<cfreturn q>
	</cffunction>



	<cffunction name="ownerassessments" output="true" access="public" returntype="any" hint="">
	<cfset var q='#arguments.bluRentUpdater.properties#'>
	<cfloop index="i" from="1" to="#arrayLen( q )#">
	<cftry>
		<cfquery name="check" datasource="contentbox">
			select 
				cb_content.contentID 
			from 
				cb_content  
			where 
				cb_content.slug=<cfqueryparam value="owner-assessments/#q[i].slug#">
		</cfquery>OA:#check.recordcount#<br>
		
			<cfquery name="u" datasource="contentbox">
				<cfif #check.recordcount# is 0>
				<!---********************* [ADD] **********************--->
					INSERT INTO
				<cfelse>
				<!---********************* [UPDATE] **********************--->
					UPDATE  							
				</cfif>
					cb_content  
				SET
					cb_content.contentType='Page',
					cb_content.isDeleted=0,
					cb_content.isPublished=1,
					cb_content.allowComments=1,
					cb_content.markup='HTML',
					cb_content.FK_authorID=1,
					cb_content.FK_parentID=42,
					cb_content.cache=1,
					cb_content.cachelayout=1,
					cb_content.cacheTimeout=0,
					cb_content.cacheLastAccessTimeout=0,
					cb_content.showInSearch=1,
					cb_content.featuredImage='',
					cb_content.featuredImageURL='',
					cb_content.HTMLKeywords='',
					cb_content.HTMLDescription='',
					cb_content.HTMLTitle='',
					cb_content.passwordProtection='',
					cb_content.publishedDate=#createODBCDate( now() )#,
					<cfif #structKeyExists( q[i], 'propname' )#>
						cb_content.title 	= <cfqueryparam value="#q[i].propname#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'slug' )#>
						cb_content.slug		= <cfqueryparam value="owner-assessments/#lCase( q[i].slug )#">,
					</cfif>
					
				<cfif #check.recordcount# eq 0>
					cb_content.createdDate=#createODBCDateTime( now() )#,
				</cfif>
					cb_content.modifiedDate=#createODBCDateTime( now() )#
				
				<cfif #check.recordcount# gt 0>
					where
						cb_content.contentID 			= 	<cfqueryparam value="#check.contentID#">
				</cfif>
			</cfquery>	
			 
		
		<cfcatch>#cfcatch.detail#OA:ERR<br></cfcatch>
		</cftry>
	</cfloop>
	<cfreturn q>
	</cffunction>




	<cffunction name="amenities" output="true" access="public" returntype="any" hint="">
	<blockquote>
	<cfset var q='#arguments.bluRentUpdater.properties#'>
	<h3>start:bluRent/DBUpdaterService/amenities</h3>
	<cfloop index="propIndex" from="1" to="#arrayLen( q )#">
	<h2>#q[propIndex].propid#:#q[propIndex].slug#</h2>

		
			<cfquery name="check" datasource="contentbox">
				UPDATE
				bluerent__properties_amenities
				set
				bluerent__properties_amenities.processed 	= 	0
				where 
				bluerent__properties_amenities.propid 	=	<cfqueryparam value="#q[propIndex].propid#">
				</cfquery>

			<ul>
			<cfif #structKeyExists( q[propIndex], 'amenities' )#>
			<cfloop index="amenityIndex" from="1" to="#arrayLen( q[propIndex]['amenities'] )#">
				<cftry>
				<li>
				<cfif #structKeyExists( q[propIndex].amenities[amenityIndex], 'amenity_id' )#>
					#q[propIndex].amenities[amenityIndex].amenity_id#:
				</cfif>
				<cfif #structKeyExists( q[propIndex].amenities[amenityIndex], 'name' )#>
					#q[propIndex].amenities[amenityIndex].name#:
				</cfif>
			<!---	#q[propIndex].amenities[amenityIndex].amenity_id#:
				#q[propIndex].amenities[amenityIndex].code#:
				#q[propIndex].amenities[amenityIndex].name#--->
				</li>
					<cfquery name="check" datasource="contentbox">
						select 
						bluerent__properties_amenities.id 
						from 
						bluerent__properties_amenities  
						where 
						bluerent__properties_amenities.amenity_id 	= 	<cfqueryparam value="#q[propIndex].amenities[amenityIndex].amenity_id#">
						and
						bluerent__properties_amenities.propid 		= 	<cfqueryparam value="#q[propIndex].propid#">
					</cfquery>
				<cfquery name="u" datasource="contentbox">
					<cfif #check.recordcount# is 0>
						insert into
					<cfelse>		
						UPDATE
					</cfif>  
						bluerent__properties_amenities  
						SET
						<cfif #structKeyExists( q[propIndex], 'propid' )#>
						bluerent__properties_amenities.propid 	= 	<cfqueryparam value="#q[propIndex].propid#">,
						</cfif>
						<cfif #structKeyExists( q[propIndex].amenities[amenityIndex], 'amenity_id' )#>
						bluerent__properties_amenities.amenity_id 	= 	<cfqueryparam value="#q[propIndex].amenities[amenityIndex].amenity_id#">,
						</cfif>
						<cfif #structKeyExists( q[propIndex].amenities[amenityIndex], 'name' )#>
						bluerent__properties_amenities.name			= 	<cfqueryparam value="#q[propIndex].amenities[amenityIndex].name#">,
						</cfif>
						bluerent__properties_amenities.updated_at 		= 	#createODBCDateTime( now() )#,
						bluerent__properties_amenities.processed		=	1
					<cfif #check.recordcount# is 0>
						,bluerent__properties_amenities.created_at		=	#createODBCDateTime( now() )#
					<cfelse>
						where
						bluerent__properties_amenities.id 				= 	<cfqueryparam value="#check.id#">
					</cfif>
				</cfquery>
				<cfcatch><h4>Error:#cfcatch.detail# #cfcatch.message#</h4></cfcatch>
				</cftry>
			</cfloop>
			</cfif>
			</ul>
			<!--- 
			<cfquery name="check" datasource="contentbox">
				DELETE FROM
				bluerent__properties_amenities
				where
				bluerent__properties_amenities.processed 	= 	0
				and 
				bluerent__properties_amenities.propid 		=	<cfqueryparam value="#q[propIndex].propid#">
			</cfquery>
		 --->
	
	</cfloop>
				

	<h3>end:bluRent/DBUpdaterService/amenities</h3>	
	</blockquote>
	<cfreturn q>
	</cffunction>










	<cffunction name="globalamenities" output="true" access="public" returntype="any" hint="">
	<cfset var q='#arguments.bluRentUpdater#'>
	
	<cfquery name="check" datasource="contentbox">
		UPDATE
			bluerent__amenities
		set
			bluerent__amenities.processed 	= 	0
	</cfquery>
	
	<cfloop index="i" from="1" to="#arrayLen( q )#">

		<!---#propid#:#q[i].amenity_id#::#q[i].code#::#q[i].name#::<br>--->
	
	<cfquery name="check" datasource="contentbox">
			select 
				bluerent__amenities.id 
			from 
				bluerent__amenities  
			where 
				bluerent__amenities.name 	= 	<cfqueryparam value="#q[i].name#">
			<cfif #structKeyExists( q[i], 'code' )#>	
			and
				bluerent__amenities.code 	= 	<cfqueryparam value="#q[i].code#">
			</cfif>
			<cfif #structKeyExists( q[i], 'ucode' )#>	
			and
				bluerent__amenities.ucode 	= 	<cfqueryparam value="#q[i].ucode#">
			</cfif>
		</cfquery>
		<cfif #check.recordcount# is 0>
		<!---********************* [ADD] **********************--->
			<cfquery name="u" datasource="contentbox">
			insert into bluerent__amenities
						(
						bluerent__amenities.name, 
						<cfif #structKeyExists( q[i], 'code' )#>
						bluerent__amenities.code, 
						</cfif>
						<cfif #structKeyExists( q[i], 'ucode' )#>
						bluerent__amenities.ucode, 	
						</cfif>
						<cfif #structKeyExists( q[i], 'category' )#>
						bluerent__amenities.category, 
						</cfif>
						bluerent__amenities.created_at, 
						bluerent__amenities.updated_at,
						bluerent__amenities.processed
						)
						values
						(	
						<cfqueryparam value="#q[i].name#">,
						<cfif #structKeyExists( q[i], 'code' )#>
							<cfqueryparam value="#q[i].code#">,
						</cfif>
						<cfif #structKeyExists( q[i], 'ucode' )#>
							<cfqueryparam value="#q[i].ucode#">,
						</cfif>
						<cfif #structKeyExists( q[i], 'category' )#>
						<cfqueryparam value="#q[i].category#">,
						</cfif>
						#createODBCDateTime( now() )#,
						#createODBCDateTime( now() )#,
						1
						)
			 </cfquery>
		<cfelse>
			<!---********************* [UPDATE] **********************--->
		 	<cfquery name="u" datasource="contentbox">
				UPDATE  
					bluerent__amenities  
				SET
					<cfif #structKeyExists( q[i], 'name' )#>
						bluerent__amenities.name 	= 	<cfqueryparam value="#q[i].name#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'code' )#>
						bluerent__amenities.code 	= 	<cfqueryparam value="#q[i].code#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'ucode' )#>
						bluerent__amenities.ucode 	= 	<cfqueryparam value="#q[i].ucode#">,
					</cfif>
					bluerent__amenities.updated_at 		= 	#createODBCDateTime( now() )#,
					bluerent__amenities.processed		=	1
				where
					bluerent__amenities.id 				= 	<cfqueryparam value="#check.id#">
			 </cfquery>								
		</cfif>

	</cfloop>
				
		<cfquery name="getAmenities" datasource="contentbox">
			select 
				*
			from 
				bluerent__amenities  
			where active = 1
		</cfquery>	
	<cfreturn getAmenities>
	</cffunction>





	<cffunction name="owners" output="true" access="public" returntype="any" hint="">
	<h3>start:bluRent/DBUpdaterService/owners</h3>
	<cfset var q='#arguments.bluRentUpdater#'>
	<cfquery name="check" datasource="contentbox">
		UPDATE
			bluerent__owners
		set
			bluerent__owners.processed 	= 	0
	</cfquery>
	<ol>
	<cfloop index="i" from="1" to="#arrayLen( q )#">
	<li>
	<ul>
		<cfif #structKeyExists( q[i], 'firstname' )#>	
			<li><b>bluerent__owners.firstname:</b>#q[i].firstname#</li>
		</cfif>
		<cfif #structKeyExists( q[i], 'lastname' )#>	
			<li><b>bluerent__owners.lastname:</b>#q[i].lastname#</li>
		</cfif>
		<cfif #structKeyExists( q[i], 'email' )#>	
			<li><b>bluerent__owners.email:</b>#q[i].email#</li>
		</cfif>
		<cfif #structKeyExists( q[i], 'address1' )#>	
			<li><b>bluerent__owners.address1:</b>#q[i].address1#</li>
		</cfif>
		<cfif #structKeyExists( q[i], 'city' )#>	
			<li><b>bluerent__owners.city:</b>#q[i].city#</li>
		</cfif>
		<cfif #structKeyExists( q[i], 'state' )#>	
			<li><b>bluerent__owners.state:</b>#q[i].state#</li>
		</cfif>
		<cfif #structKeyExists( q[i], 'zip' )#>	
			<li><b>bluerent__owners.zip:</b>#q[i].zip#</li>
		</cfif>
		<cfif #structKeyExists( q[i], 'homephone' )#>
			<li><b>bluerent__owners.homephone:</b>#q[i].homephone#</li>
		</cfif>
		<cfif #structKeyExists( q[i], 'workphone' )#>
			<li><b>bluerent__owners.workphone:</b>#q[i].workphone#</li>
		</cfif>
		<cfif #structKeyExists( q[i], 'cellphone' )#>
			<li><b>bluerent__owners.cellphone:</b>#q[i].cellphone#</li>
		</cfif>
		<cfif #structKeyExists( q[i], 'email' )#>
			<li><b>bluerent__owners.email:</b>#q[i].email#</li>
		</cfif>
		<cfif #structKeyExists( q[i], 'company' )#>
			<li><b>bluerent__owners.company:</b>#q[i].company#</li>
		</cfif>
	</ul>
	</li>
	<cfquery name="check" datasource="contentbox">
			select 
				bluerent__owners.id 
			from 
				bluerent__owners  
			where 
				bluerent__owners.firstname 	= 	<cfqueryparam value="#q[i].firstname#">
			<cfif #structKeyExists( q[i], 'lastname' )#>	
			and
				bluerent__owners.lastname 	= 	<cfqueryparam value="#q[i].lastname#">
			</cfif>
			<cfif #structKeyExists( q[i], 'vendorid' )#>	
			and
				bluerent__owners.vendorid 	= 	<cfqueryparam value="#q[i].vendorid#">
			</cfif>
		</cfquery>
		
			<!---********************* [UPDATE] **********************--->
		 	<cfquery name="u" datasource="contentbox">
			<cfif #check.recordcount# is 0>
				INSERT INTO
				<cfelse>
				UPDATE  
			</cfif>
					bluerent__owners  
				
				SET
					<cfif #structKeyExists( q[i], 'vendorid' )#>
						bluerent__owners.vendorid 	= 	<cfqueryparam value="#q[i].vendorid#" cfsqltype="cf_sql_varchar">,
					</cfif>
					<cfif #structKeyExists( q[i], 'vendorownerid' )#>
						bluerent__owners.vendorownerid 	= 	<cfqueryparam value="#q[i].vendorownerid#" cfsqltype="cf_sql_varchar">,
					</cfif>
					<cfif #structKeyExists( q[i], 'vendorgid' )#>
						bluerent__owners.vendorgid 	= 	<cfqueryparam value="#q[i].vendorgid#" cfsqltype="cf_sql_varchar">,
					</cfif>
					<cfif #structKeyExists( q[i], 'firstname' )#>
						bluerent__owners.firstname 	= 	<cfqueryparam value="#q[i].firstname#" cfsqltype="cf_sql_varchar">,
					</cfif>
					<cfif #structKeyExists( q[i], 'lastname' )#>
						bluerent__owners.lastname 	= 	<cfqueryparam value="#q[i].lastname#" cfsqltype="cf_sql_varchar">,
					</cfif>
					<cfif #structKeyExists( q[i], 'address1' )#>
						bluerent__owners.address1 	= 	<cfqueryparam value="#q[i].address1#" cfsqltype="cf_sql_varchar">,
					</cfif>
					<cfif #structKeyExists( q[i], 'address2' )#>
						bluerent__owners.address2 	= 	<cfqueryparam value="#q[i].address2#" cfsqltype="cf_sql_varchar">,
					</cfif>
					<cfif #structKeyExists( q[i], 'city' )#>
						bluerent__owners.city 	= 	<cfqueryparam value="#q[i].city#" cfsqltype="cf_sql_varchar">,
					</cfif>
					<cfif #structKeyExists( q[i], 'state' )#>
						bluerent__owners.state 	= 	<cfqueryparam value="#q[i].state#" cfsqltype="cf_sql_varchar">,
					</cfif>
					<cfif #structKeyExists( q[i], 'zip' )#>
						bluerent__owners.zip 	= 	<cfqueryparam value="#q[i].zip#" cfsqltype="cf_sql_varchar">,
					</cfif>
					<cfif #structKeyExists( q[i], 'homephone' )#>
						bluerent__owners.homephone 	= 	<cfqueryparam value="#q[i].homephone#" cfsqltype="cf_sql_varchar">,
					</cfif>
					<cfif #structKeyExists( q[i], 'workphone' )#>
						bluerent__owners.workphone 	= 	<cfqueryparam value="#q[i].workphone#" cfsqltype="cf_sql_varchar">,
					</cfif>
					<cfif #structKeyExists( q[i], 'cellphone' )#>
						bluerent__owners.cellphone 	= 	<cfqueryparam value="#q[i].cellphone#" cfsqltype="cf_sql_varchar">,
					</cfif>
					<cfif #structKeyExists( q[i], 'email' )#>
						bluerent__owners.email 	= 	<cfqueryparam value="#q[i].email#" cfsqltype="cf_sql_varchar">,
					</cfif>
					<cfif #structKeyExists( q[i], 'company' )#>
						bluerent__owners.company 	= 	<cfqueryparam value="#q[i].company#" cfsqltype="cf_sql_varchar">,
					</cfif>
					<cfif #check.recordcount# is 0>
						bluerent__owners.created_at 	= 	#createODBCDateTime( now() )#,
					</cfif>
					bluerent__owners.updated_at 		= 	#createODBCDateTime( now() )#,
					bluerent__owners.processed		=	1
				<cfif #check.recordcount# gt 0>	
				where
					bluerent__owners.id 				= 	<cfqueryparam value="#check.id#">
				</cfif>
			 </cfquery>								
			
	</cfloop>
	</ol>		
		<cfquery name="get" datasource="contentbox">
			select 
				*
			from 
				bluerent__owners  
				where active=1
		</cfquery>
		<h3>end:bluRent/DBUpdaterService/owners</h3>	
	<cfreturn get>
	</cffunction>



	<cffunction name="tenants" output="true" access="public" returntype="any" hint="">
	<h3>start:bluRent/DBUpdaterService/tenants</h3>
	<cfsetting requesttimeout="2000" />
	<cfset var q='#arguments.bluRentUpdater#'>
	
	<cfquery name="check" datasource="contentbox">
		UPDATE
			bluerent__tenants
		set
			bluerent__tenants.processed 	= 	0
	</cfquery>
	<ol>
	<cfloop index="i" from="1" to="#arrayLen( q )#">
	<li>
	<ul>
		<cfif #structKeyExists( q[i], 'firstname' )#>	
			<li><b>bluerent__tenants.firstname:</b>#q[i].firstname#</li>
		</cfif>
		<cfif #structKeyExists( q[i], 'lastname' )#>	
			<li><b>bluerent__tenants.lastname:</b>#q[i].lastname#</li>
		</cfif>
		<cfif #structKeyExists( q[i], 'email' )#>	
			<li><b>bluerent__tenants.email:</b>#q[i].email#</li>
		</cfif>
		<cfif #structKeyExists( q[i], 'address1' )#>	
			<li><b>bluerent__tenants.address1:</b>#q[i].address1#</li>
		</cfif>
		<cfif #structKeyExists( q[i], 'address2' )#>	
			<li><b>bluerent__tenants.address2:</b>#q[i].address2#</li>
		</cfif>
		<cfif #structKeyExists( q[i], 'city' )#>	
			<li><b>bluerent__tenants.city:</b>#q[i].city#</li>
		</cfif>
		<cfif #structKeyExists( q[i], 'state' )#>	
			<li><b>bluerent__tenants.state:</b>#q[i].state#</li>
		</cfif>
		<cfif #structKeyExists( q[i], 'zip' )#>	
			<li><b>bluerent__tenants.zip:</b>#q[i].zip#</li>
		</cfif>
		<cfif #structKeyExists( q[i], 'homephone' )#>
			<li><b>bluerent__tenants.homephone:</b>#q[i].homephone#</li>
		</cfif>
		<cfif #structKeyExists( q[i], 'workphone' )#>
			<li><b>bluerent__tenants.workphone:</b>#q[i].workphone#</li>
		</cfif>
		<cfif #structKeyExists( q[i], 'cellphone' )#>
			<li><b>bluerent__tenants.cellphone:</b>#q[i].cellphone#</li>
		</cfif>
		<cfif #structKeyExists( q[i], 'email' )#>
			<li><b>bluerent__tenants.email:</b>#q[i].email#</li>
		</cfif>
		<cfif #structKeyExists( q[i], 'company' )#>
			<li><b>bluerent__tenants.company:</b>#q[i].company#</li>
		</cfif>
	</ul>
	</li>
	<cfquery name="check" datasource="contentbox">
			select 
				bluerent__tenants.id 
			from 
				bluerent__tenants  
			where 
				bluerent__tenants.firstname 	= 	<cfqueryparam value="#q[i].firstname#">
			<cfif #structKeyExists( q[i], 'lastname' )#>	
			and
				bluerent__tenants.lastname 	= 	<cfqueryparam value="#q[i].lastname#">
			</cfif>
			<cfif #structKeyExists( q[i], 'vendorid' )#>	
			and
				bluerent__tenants.vendorid 	= 	<cfqueryparam value="#q[i].vendorid#">
			</cfif>
		</cfquery>
		
			<!---********************* [UPDATE] **********************--->
		 	<cfquery name="u" datasource="contentbox">
			<cfif #check.recordcount# is 0>
				INSERT INTO
				<cfelse>
				UPDATE  
			</cfif>
					bluerent__tenants  
				
				SET
					<cfif #structKeyExists( q[i], 'vendorid' )#>
						bluerent__tenants.vendorid 	= 	<cfqueryparam value="#q[i].vendorid#" cfsqltype="cf_sql_varchar">,
					</cfif>
					<cfif #structKeyExists( q[i], 'vendorcontactid' )#>
						bluerent__tenants.vendorcontactid 	= 	<cfqueryparam value="#q[i].vendorcontactid#" cfsqltype="cf_sql_varchar">,
					</cfif>
					<cfif #structKeyExists( q[i], 'vendorgid' )#>
						bluerent__tenants.vendorgid 	= 	<cfqueryparam value="#q[i].vendorgid#" cfsqltype="cf_sql_varchar">,
					</cfif>
					<cfif #structKeyExists( q[i], 'firstname' )#>
						bluerent__tenants.firstname 	= 	<cfqueryparam value="#q[i].firstname#" cfsqltype="cf_sql_varchar">,
					</cfif>
					<cfif #structKeyExists( q[i], 'lastname' )#>
						bluerent__tenants.lastname 	= 	<cfqueryparam value="#q[i].lastname#" cfsqltype="cf_sql_varchar">,
					</cfif>
					<cfif #structKeyExists( q[i], 'address1' )#>
						bluerent__tenants.address1 	= 	<cfqueryparam value="#q[i].address1#" cfsqltype="cf_sql_varchar">,
					</cfif>
					<cfif #structKeyExists( q[i], 'address2' )#>
						bluerent__tenants.address2 	= 	<cfqueryparam value="#q[i].address2#" cfsqltype="cf_sql_varchar">,
					</cfif>
					<cfif #structKeyExists( q[i], 'city' )#>
						bluerent__tenants.city 	= 	<cfqueryparam value="#q[i].city#" cfsqltype="cf_sql_varchar">,
					</cfif>
					<cfif #structKeyExists( q[i], 'state' )#>
						bluerent__tenants.state 	= 	<cfqueryparam value="#q[i].state#" cfsqltype="cf_sql_varchar">,
					</cfif>
					<cfif #structKeyExists( q[i], 'zip' )#>
						bluerent__tenants.zip 	= 	<cfqueryparam value="#q[i].zip#" cfsqltype="cf_sql_varchar">,
					</cfif>
					<cfif #structKeyExists( q[i], 'homephone' )#>
						bluerent__tenants.homephone 	= 	<cfqueryparam value="#q[i].homephone#" cfsqltype="cf_sql_varchar">,
					</cfif>
					<cfif #structKeyExists( q[i], 'workphone' )#>
						bluerent__tenants.workphone 	= 	<cfqueryparam value="#q[i].workphone#" cfsqltype="cf_sql_varchar">,
					</cfif>
					<cfif #structKeyExists( q[i], 'cellphone' )#>
						bluerent__tenants.cellphone 	= 	<cfqueryparam value="#q[i].cellphone#" cfsqltype="cf_sql_varchar">,
					</cfif>
					<cfif #structKeyExists( q[i], 'email' )#>
						bluerent__tenants.email 	= 	<cfqueryparam value="#q[i].email#" cfsqltype="cf_sql_varchar">,
					</cfif>
					<cfif #structKeyExists( q[i], 'company' )#>
						bluerent__tenants.company 	= 	<cfqueryparam value="#q[i].company#" cfsqltype="cf_sql_varchar">,
					</cfif>
					<cfif #check.recordcount# is 0>
						bluerent__tenants.created_at 	= 	#createODBCDateTime( now() )#,
					</cfif>
					bluerent__tenants.updated_at 		= 	#createODBCDateTime( now() )#,
					bluerent__tenants.processed		=	1
				<cfif #check.recordcount# gt 0>	
				where
					bluerent__tenants.id 				= 	<cfqueryparam value="#check.id#">
				</cfif>
			 </cfquery>								

	</cfloop>
	</ol>
		<!--- 	
		<cfquery name="check" datasource="contentbox">
			DELETE FROM
				bluerent__tenants
			where
				bluerent__tenants.processed 	= 	0
			
		</cfquery>
		 --->
		<!--- disable the untouched and enable the touched photo records --->
		<cfquery name="enableProcessedAmen" datasource="contentbox">
			UPDATE bluerent__tenants
			SET
				active = 1
			WHERE
				processed = 1
		</cfquery>
		<cfquery name="disableUntouchedAmen" datasource="contentbox">
			UPDATE bluerent__tenants
			SET
				active = 0
			WHERE
				processed = 0
		</cfquery>
		<cfquery name="get" datasource="contentbox">
			select 
				*
			from 
				bluerent__tenants
			where active = 1
		</cfquery>
		<h3>end:bluRent/DBUpdaterService/tenants</h3>	
	<cfreturn get>
	</cffunction>











	<cffunction name="avail" output="true" access="public" returntype="any" hint="">
	<cfset var q='#arguments.bluRentUpdater#'>
	<blockquote>
	<h3>start:bluRent/DBUpdaterService/avail</h3>
	
	<cfquery name="check" datasource="contentbox">
		UPDATE
			bluerent__avails
		set
			bluerent__avails.processed 	= 	0
	</cfquery>
	<ol>
	<cfloop index="i" from="1" to="#arrayLen( q )#">
	<li>
		<ul>
			<cfif #structKeyExists( q[i], 'propid' )#>	
			<li><b>bluerent__avails.propid:</b>#q[i].propid#</li>
			</cfif>
			<cfif #structKeyExists( q[i], 'vendorresnum' )#>
			<li><b>bluerent__avails.vendorresnum:</b>#q[i].vendorresnum#</li>
			</cfif>
			<cfif #structKeyExists( q[i], 'arrive' )#>	
			<li><b>bluerent__avails.arrivedate:</b>#q[i].arrive#</li>
			</cfif>
			<cfif #structKeyExists( q[i], 'depart' )#>	
			<li><b>bluerent__avails.departdate:</b>#q[i].depart#</li>
			</cfif>
			<cfif #structKeyExists( q[i], 'firstname' )#>
			<li><b>bluerent__avails.firstname:</b>#q[i].firstname#</li>
			</cfif>
			<cfif #structKeyExists( q[i], 'lastname' )#>
			<li><b>bluerent__avails.lastname:</b>#q[i].lastname#</li>
			</cfif>
		</ul>
	</li>
	<cfquery name="check" datasource="contentbox">
			select 
				bluerent__avails.id 
			from 
				bluerent__avails  
			where 
				bluerent__avails.propid 	= 	<cfqueryparam value="#q[i].propid#">
			and
				bluerent__avails.arrivedate = 	<cfqueryparam value="#q[i].arrive#" cfsqltype="CF_SQL_DATE">
			and
				bluerent__avails.departdate = 	<cfqueryparam value="#q[i].depart#" cfsqltype="CF_SQL_DATE">
			<cfif #structKeyExists( q[i], 'vendorstatus' )#>
			and
			bluerent__avails.vendorstatus = 	<cfqueryparam value="#q[i].vendorstatus#" >
			</cfif>
			<cfif #structKeyExists( q[i], 'vendorresnum' )#>
			and
			bluerent__avails.vendorresnum = 	<cfqueryparam value="#q[i].vendorresnum#" >
			</cfif>
		</cfquery>
	
		 	<cfquery name="u" datasource="contentbox">
		 	<cfif #check.recordcount# is 0>
		 	insert into
		 		<cfelse>
				UPDATE  
				</cfif>
					bluerent__avails  
				SET
					<cfif #structKeyExists( q[i], 'propid' )#>
						bluerent__avails.propid 		= 	<cfqueryparam value="#q[i].propid#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'propid' )#>
						bluerent__avails.vendorid 		= 	<cfqueryparam value="#q[i].propid#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'arrive' )#>
						bluerent__avails.arrivedate 	= 	<cfqueryparam value="#createODBCDate( q[i].arrive )#" cfsqltype="CF_SQL_DATE">,
					</cfif>
					<cfif #structKeyExists( q[i], 'depart' )#>
						bluerent__avails.departdate 	= 	<cfqueryparam value="#createODBCDate( q[i].depart )#" cfsqltype="CF_SQL_DATE">,
					</cfif>
					<cfif #structKeyExists( q[i], 'vendorresnum' )#>
						bluerent__avails.vendorresnum 		= 	<cfqueryparam value="#q[i].vendorresnum#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'vendortype' )#>
						bluerent__avails.vendortype 		= 	<cfqueryparam value="#q[i].vendortype#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'vendorstatus' )#>
						bluerent__avails.vendorstatus 		= 	<cfqueryparam value="#q[i].vendorstatus#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'firstname' )#>
						bluerent__avails.firstname 		= 	<cfqueryparam value="#q[i].firstname#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'lastname' )#>
						bluerent__avails.lastname 		= 	<cfqueryparam value="#q[i].lastname#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'email' )#>
						bluerent__avails.email 		= 	<cfqueryparam value="#q[i].email#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'address1' )#>
						bluerent__avails.address1 		= 	<cfqueryparam value="#q[i].address1#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'address2' )#>
						bluerent__avails.address2 		= 	<cfqueryparam value="#q[i].address2#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'city' )#>
						bluerent__avails.city 		= 	<cfqueryparam value="#q[i].city#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'state' )#>
						bluerent__avails.state 		= 	<cfqueryparam value="#q[i].state#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'zip' )#>
						bluerent__avails.zip 		= 	<cfqueryparam value="#q[i].zip#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'homephone' )#>
						bluerent__avails.homephone 		= 	<cfqueryparam value="#q[i].homephone#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'cellphone' )#>
						bluerent__avails.cellphone 		= 	<cfqueryparam value="#q[i].cellphone#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'workphone' )#>
						bluerent__avails.workphone 		= 	<cfqueryparam value="#q[i].workphone#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'notes' )#>
						bluerent__avails.notes 		= 	<cfqueryparam value="#q[i].notes#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'adults' )#>
						bluerent__avails.numadults 		= 	<cfqueryparam value="#q[i].adults#">,
					</cfif>

					
					bluerent__avails.updated_at 		= 	#createODBCDateTime( now() )#,
					bluerent__avails.processed			=	1
				<cfif #check.recordcount# is 0>
					<cfif #structKeyExists( q[i], 'reservationdate' )#>
						,bluerent__avails.created_at 		= 	<cfqueryparam value="#q[i].reservationdate#" cfsqltype="CF_SQL_DATE">
					<cfelse>
						,bluerent__avails.created_at 		= 	#createODBCDateTime( now() )#
					</cfif>
					
				<cfelse>
				where
					bluerent__avails.id =	<cfqueryparam value="#check.id#" cfsqltype="cf_sql_integer">
				</cfif>
			 </cfquery>								
		
		
	</cfloop>
	</ol>
	
		<h3>end:bluRent/DBUpdaterService/avail</h3>
		</blockquote>
	<cfreturn q>
	</cffunction>




	<cffunction name="seasonsFromRates" output="true" access="public" returntype="any" hint="">
	<blockquote>
	<cfset var q='#arguments.bluRentUpdater#'>
	<h3>start:bluRent/DBUpdaterService/seasonsFromRates</h3>
	
	<cfquery name="check" datasource="contentbox">
		UPDATE
			bluerent__seasons
		set
			bluerent__seasons.processed 	= 	0
	</cfquery>
	
	<cfloop index="i" from="1" to="#arrayLen( q )#">
		<cfquery name="check" datasource="contentbox">
			select 
				bluerent__seasons.id 
			from 
				bluerent__seasons  
			where 
				bluerent__seasons.seasonstart 	= 	<cfqueryparam value="#q[i].start#" cfsqltype="CF_SQL_DATE">
			and
				bluerent__seasons.seasonend 	= 	<cfqueryparam value="#q[i].end#" cfsqltype="CF_SQL_DATE">
			and
				bluerent__seasons.season 		= 	<cfqueryparam value="#q[i].season#">
		</cfquery>
	 	<cfquery name="u" datasource="contentbox">
		 	<cfif #check.recordcount# is 0>
		 	insert into
		 		<cfelse>
				UPDATE  
				</cfif>
					bluerent__seasons  
				SET
					<cfif #structKeyExists( q[i], 'start' )#>
						bluerent__seasons.seasonstart 	= 	<cfqueryparam value="#createODBCDate( q[i].start )#" cfsqltype="CF_SQL_DATE">,
					</cfif>
					<cfif #structKeyExists( q[i], 'end' )#>
						bluerent__seasons.seasonend 	= 	<cfqueryparam value="#createODBCDate( q[i].end )#" cfsqltype="CF_SQL_DATE">,
					</cfif>
					<cfif #structKeyExists( q[i], 'season' )#>
						bluerent__seasons.season 	= 	<cfqueryparam value="#q[i].season#">,
					</cfif>
					bluerent__seasons.updated_at 		= 	#createODBCDateTime( now() )#,
					bluerent__seasons.processed			=	1,
					bluerent__seasons.active			=	1
				<cfif #check.recordcount# is 0>
					,bluerent__seasons.created_at 		= 	#createODBCDateTime( now() )#
				<cfelse>
				where
					bluerent__seasons.id 				= 	<cfqueryparam value="#check.id#">
					</cfif>
		 </cfquery>								
	</cfloop>
	<cfquery name="getSeasons" datasource="contentbox">
			select 
				*
			from 
			bluerent__seasons
	</cfquery>
	
	<h3>end:bluRent/DBUpdaterService/seasonsFromRates</h3>
	</blockquote>
	<cfreturn getSeasons>
	
	</cffunction>






	<cffunction name="rates" output="true" access="public" returntype="any" hint="">
	<blockquote>
	<cfset var q='#arguments.bluRentUpdater#'>
	<h3>start:bluRent/DBUpdaterService/rates</h3>
	
	<cfquery name="check" datasource="contentbox">
		UPDATE
			bluerent__rates
		set
			bluerent__rates.processed 	= 	0
	</cfquery>
	<ol>
	<cfloop index="i" from="1" to="#arrayLen( q )#">
	<li>
	<ul>
		<cfif #structKeyExists( q[i], 'propid' )#>
		<li><b>bluerent__rates.propid:</b>#q[i].propid#</li>
		</cfif>
		<cfif #structKeyExists( q[i], 'seasonID' )#>
		<li><b>bluerent__rates.season_id:</b>#q[i].seasonID#</li>
		</cfif>
		<cfif #structKeyExists( q[i], 'daily' )#>
		<li><b>bluerent__rates.daily:</b>#q[i].daily#</li>
		</cfif>
		<cfif #structKeyExists( q[i], 'nightly' )#>
		<li><b>bluerent__rates.daily:</b>#q[i].nightly#</li>
		</cfif>
		<cfif #structKeyExists( q[i], 'weekly' )#>
		<li><b>bluerent__rates.weekly:</b>#q[i].weekly#</li>
		</cfif>
		<cfif #structKeyExists( q[i], 'minstay' )#>
		<li><b>bluerent__rates.minstay:</b>#q[i].minstay#</li>
		</cfif>
		<cfif #structKeyExists( q[i], 'minstayrate' )#>
		<li><b>bluerent__rates.minstayrate:</b>#q[i].minstayrate#</li>
		</cfif>
	</ul>
	</li>
		<cfquery name="check" datasource="contentbox">
			select 
				bluerent__rates.id 
			from 
				bluerent__rates  
			where 
				bluerent__rates.propid 		= 	<cfqueryparam value="#q[i].propid#">
			and
				bluerent__rates.season_id 	= 	<cfqueryparam value="#q[i].seasonID#">
		</cfquery>
	 	<cfquery name="u" datasource="contentbox">
		 	<cfif #check.recordcount# is 0>
		 	insert into
		 		<cfelse>
				UPDATE  
				</cfif>
					bluerent__rates  
				SET
					<cfif #structKeyExists( q[i], 'propid' )#>
						bluerent__rates.propid 	= 	<cfqueryparam value="#q[i].propid#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'seasonID' )#>
						bluerent__rates.season_id 	= 	<cfqueryparam value="#q[i].seasonID#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'daily' )#>
						bluerent__rates.daily 	= 	<cfqueryparam value="#q[i].daily#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'nightly' )#>
						bluerent__rates.daily 	= 	<cfqueryparam value="#q[i].nightly#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'weekly' )#>
						bluerent__rates.weekly 	= 	<cfqueryparam value="#q[i].weekly#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'minstay' )#>
						bluerent__rates.minstay 	= 	<cfqueryparam value="#q[i].minstay#">,
					</cfif>
					<cfif #structKeyExists( q[i], 'minstayrate' )#>
						bluerent__rates.minstayrate 	= 	<cfqueryparam value="#q[i].minstayrate#">,
					</cfif>
					bluerent__rates.updated_at 		= 	#createODBCDateTime( now() )#,
					bluerent__rates.processed			=	1
				<cfif #check.recordcount# is 0>
					,bluerent__rates.created_at 		= 	#createODBCDateTime( now() )#
				<cfelse>
				where
					bluerent__rates.id 				= 	<cfqueryparam value="#check.id#">
					</cfif>
		 </cfquery>								
	</cfloop>
	</ol>
	<cfquery name="getRates" datasource="contentbox">
			select 
				*
			from 
			bluerent__rates
	</cfquery>
		
	<h3>end:bluRent/DBUpdaterService/rates</h3>
	</blockquote>
	
	
	<cfreturn getRates>
	</cffunction>








	<cffunction name="getLocations" output="true" access="public" returntype="any" hint="">
		<blockquote>
		<h3>start:bluRent/DBUpdaterService/getLocations</h3>
		<cfquery name="qLocations" datasource="contentbox">
			SELECT DISTINCT
				bluerent__properties.location 
		 	FROM
		 		bluerent__properties
	 		where active = 1
	 		and CHAR_LENGTH(bluerent__properties.location) > 0
	 		ORDER BY  
		 		bluerent__properties.location 
		</cfquery>
		#valuelist(qLocations.location)#
		<h3>end:bluRent/DBUpdaterService/getLocations</h3>
		</blockquote>
		<cfreturn valueList( qLocations.location )>
	</cffunction>


	<cffunction name="getAreas" output="true" access="public" returntype="any" hint="">
		<blockquote>
		<h3>start:bluRent/DBUpdaterService/getAreas</h3>
		<cfquery name="qAreas" datasource="contentbox">
			SELECT DISTINCT
				bluerent__properties.area 
		 	FROM
		 		bluerent__properties
	 		where active = 1
	 		and CHAR_LENGTH(bluerent__properties.area) > 0
	 		ORDER BY  
		 		bluerent__properties.area 
		</cfquery>
		#valuelist(qAreas.area)#
		<h3>end:bluRent/DBUpdaterService/getAreas</h3>
		</blockquote>
		<cfreturn valueList( qAreas.area )>
	</cffunction>

	<cffunction name="getViews" output="true" access="public" returntype="any" hint="">
		<blockquote>
		<h3>start:bluRent/DBUpdaterService/getViews</h3>
		<cfquery name="qViews" datasource="contentbox">
			SELECT DISTINCT
				bluerent__properties.view 
		 	FROM
		 		bluerent__properties
		 	where active = 1
		 	and CHAR_LENGTH(bluerent__properties.view) > 0
	 		ORDER BY  
		 		bluerent__properties.view 
		</cfquery>
		#valuelist(qViews.view)#
		<h3>end:bluRent/DBUpdaterService/getViews</h3>
		</blockquote>
		<cfreturn valueList( qViews.view )>
	</cffunction>

	<cffunction name="getTypes" output="true" access="public" returntype="any" hint="">
		<blockquote>
		<h3>start:bluRent/DBUpdaterService/getTypes</h3>
		<cfquery name="qTypes" datasource="contentbox">
			SELECT DISTINCT
				bluerent__properties.type 
		 	FROM
		 		bluerent__properties
	 		where active = 1
	 		and CHAR_LENGTH(bluerent__properties.type) > 0
	 		ORDER BY  
		 		bluerent__properties.type 
		</cfquery>
		#valuelist(qTypes.type)#
		<h3>end:bluRent/DBUpdaterService/getTypes</h3>
		</blockquote>
		<cfreturn valueList( qTypes.type )>
	</cffunction>

	<cffunction name="getCommunities" output="true" access="public" returntype="any" hint="">
		<blockquote>
		<h3>start:bluRent/DBUpdaterService/getCommunities</h3>
		<cfquery name="qCommunities" datasource="contentbox">
			SELECT DISTINCT
				bluerent__properties.community 
		 	FROM
		 		bluerent__properties
	 		where active = 1 
	 		and CHAR_LENGTH(bluerent__properties.community) > 0
	 		ORDER BY  
		 		bluerent__properties.community
		</cfquery>
		#valuelist(qCommunities.community)#
		<h3>end:bluRent/DBUpdaterService/getCommunities</h3>
		</blockquote>
		<cfreturn valueList( qCommunities.community )>
	</cffunction>

</cfcomponent>