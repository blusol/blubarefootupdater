<cfcomponent output='true' singleton>
<cfsetting requesttimeout="300" />	
	<!--- Dependencies --->


	<cfproperty name="settingService" 	inject="id:settingService@bluBarefootUpdater">


	<cfscript>
		this.barefooturl='https://portals.barefoot.com/barefootwebservice/BarefootService.asmx';
		this.soapactionroot='http://www.barefoot.com/Services/';
		

	</cfscript>

	

	<!--- index --->
    <cffunction name="index" output="true" access="public" returntype="any" hint="">
    	<cfset var q = ''>
    	
		<cfreturn q>
    </cffunction>


    <!--- GetPropertyAllImgsXML --->
    <cffunction name="GetPropertyAllImgsXML" output="true" access="public" returntype="any" hint="">
    <blockquote>
	<h2>START:bluBarefootUpdater/BarefootService/GetPropertyAllImgsXML</h2>
    <cfsetting requesttimeout="300">
    	<cfset var q = ''>
			<cfsavecontent variable="this.barefootpost"><?xml version="1.0" encoding="utf-8"?>
			<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
			  <soap:Body>
			    <GetPropertyAllImgsXML xmlns="http://www.barefoot.com/Services/">
			      <username>#arguments.prc.barefoot.username#</username>
			      <password>#arguments.prc.barefoot.password#</password>
			      <barefootAccount>#arguments.prc.barefoot.barefootaccount#</barefootAccount>
			      <propertyId>#arguments.propid#</propertyId>
			    </GetPropertyAllImgsXML>
			  </soap:Body>
			</soap:Envelope>
			</cfsavecontent>
			<cfhttp url="#this.barefooturl#" method="post">
				<cfhttpparam name="Host" type="HEADER" value="agent.barefoot.com">
				<cfhttpparam name="Cache-Control" type="HEADER" value="no-cache">
				<cfhttpparam name="Content-Type" type="HEADER" value="text/xml; charset=utf-8">
				<cfhttpparam name="SOAPAction" type="HEADER" value='"#this.soapactionroot#GetPropertyAllImgsXML"'>
				<cfhttpparam name="" type="body" value="#this.barefootpost#">
			</cfhttp>
				<cfset q = xmlParse( #cfhttp.fileContent# )>
				<cfreturn xmlParse( q.Envelope.Body.GetPropertyAllImgsXMLResponse.GetPropertyAllImgsXMLResult.XmlText )>
		<h2>START:bluBarefootUpdater/BarefootService/GetPropertyAllImgsXML</h2>
		</blockquote>
		<cfreturn q>   
    </cffunction>

	<!--- GetPropertyAmmenityNameXML --->
	<cffunction name="GetPropertyAmmenityNameXML" output="true" access="public" returntype="any" hint="">
	<cfset var q = ''>
	<cfset myQuery = queryNew( 'code, type, name', 'VarChar, VarChar, VarChar' )>

		<cfsavecontent variable="this.barefootpost"><?xml version="1.0" encoding="utf-8"?>
		<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
		  <soap:Body>
		    <GetPropertyAmmenityNameXML xmlns="http://www.barefoot.com/Services/">
		      <username>#arguments.prc.barefoot.username#</username>
		      <password>#arguments.prc.barefoot.password#</password>
		      <barefootAccount>#arguments.prc.barefoot.barefootaccount#</barefootAccount>
		      <num></num>
		    </GetPropertyAmmenityNameXML>
		  </soap:Body>
		</soap:Envelope>
		</cfsavecontent>
		<cfhttp url="#this.barefooturl#" method="post">
			<cfhttpparam name="Host" type="HEADER" value="agent.barefoot.com">
			<cfhttpparam name="Cache-Control" type="HEADER" value="no-cache">
			<cfhttpparam name="Content-Type" type="HEADER" value="text/xml; charset=utf-8">
			<cfhttpparam name="SOAPAction" type="HEADER" value='"#this.soapactionroot#GetPropertyAmmenityNameXML"'>
			<cfhttpparam name="" type="body" value="#this.barefootpost#">
		</cfhttp>
		<cfset q = xmlParse( #cfhttp.fileContent# )>
	<cfreturn xmlParse( q.Envelope.Body.GetPropertyAmmenityNameXMLResponse.GetPropertyAmmenityNameXMLResult.XmlText )>
	</cffunction>
	
	<!--- GetProperty --->
    <cffunction name="GetProperty" output="true" access="public" returntype="any" hint="">
		<cfset var q = ''>
		<blockquote>
		<h2>START:bluBarefootUpdater/BarefootService/GetProperty</h2>
		<cfsetting requesttimeout="90">
			<cfsavecontent variable="this.barefootpost"><?xml version="1.0" encoding="utf-8"?>
			<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
			  <soap:Body>
			    <GetProperty xmlns="http://www.barefoot.com/Services/">
			      <username>#arguments.prc.barefoot.username#</username>
			      <password>#arguments.prc.barefoot.password#</password>
			      <barefootAccount>#arguments.prc.barefoot.barefootaccount#</barefootAccount>
			    </GetProperty>
			  </soap:Body>
			</soap:Envelope>
			</cfsavecontent>
			<cfhttp url="#this.barefooturl#?#randRange( 100000, 99999999 )#" method="post">
				<cfhttpparam name="Host" type="HEADER" value="agent.barefoot.com">
				<cfhttpparam name="Cache-Control" type="HEADER" value="no-cache">
				<cfhttpparam name="Content-Type" type="HEADER" value="text/xml; charset=utf-8">
				<cfhttpparam name="SOAPAction" type="HEADER" value='"#this.soapactionroot#GetProperty"'>
				<cfhttpparam name="" type="body" value="#this.barefootpost#">
			</cfhttp>
			<cfset qstring = xmlParse( #cfhttp.fileContent# )>
			<cfset q = xmlParse( qstring.envelope.body.GetPropertyResponse.GetPropertyResult.XmlText )>
			<cfset q=xmlParse( q )>
			
		<h2>END:bluBarefootUpdater/BarefootService/GetProperty</h2>
		</blockquote>
		
		<cfreturn q.PropertyList.Property>
    </cffunction>



    <!--- GetPropertyBookingDateForAllXML --->
    <cffunction name="GetPropertyBookingDateForAllXML" output="true" access="public" returntype="any" hint="">
    <blockquote>
    <h2>START:bluBarefootUpdater/BarefootService/GetPropertyBookingDateForAllXML</h2>
		<cfset var q = ''>
		<Cfset this.mystart='#dateFormat( now(), 'yyyy-mm' )#-01'>
<Cfset this.mystart='#dateFormat(
	'#dateAdd(
		'd',
		-90,
		this.mystart
	)#',
	'yyyy-mm-dd'
)#'>
<Cfset this.myend='#dateFormat(
	'#dateAdd(
		'd',
		635,
		this.mystart
	)#',
	'yyyy-mm-dd'
)#'>
		<cfsetting requesttimeout="360">
			<cfsavecontent variable="this.barefootpost"><?xml version="1.0" encoding="utf-8"?>
			<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
			  <soap:Body>
			    <GetPropertyBookingDateForAllXML xmlns="http://www.barefoot.com/Services/">
			      <username>#arguments.prc.barefoot.username#</username>
			      <password>#arguments.prc.barefoot.password#</password>
			      <barefootAccount>#arguments.prc.barefoot.barefootaccount#</barefootAccount>
			      
			      <date1>#this.mystart#</date1>
    			  <date2>#this.myend#</date2>
			    </GetPropertyBookingDateForAllXML>
			  </soap:Body>
			</soap:Envelope>
			</cfsavecontent>
			<cfhttp url="#this.barefooturl#" method="post">
				<cfhttpparam name="Host" type="HEADER" value="agent.barefoot.com">
				<cfhttpparam name="Cache-Control" type="HEADER" value="no-cache">
				<cfhttpparam name="Content-Type" type="HEADER" value="text/xml; charset=utf-8">
				<cfhttpparam name="SOAPAction" type="HEADER" value='"#this.soapactionroot#GetPropertyBookingDateForAllXML"'>
				<cfhttpparam name="" type="body" value="#this.barefootpost#">
			</cfhttp>
			<cfset q = xmlParse( #cfhttp.fileContent# ).envelope.body.GetPropertyBookingDateForAllXMLResponse.GetPropertyBookingDateForAllXMLResult.XmlText>
			<cfxml variable="q">#q#</cfxml>
<h2>END:bluBarefootUpdater/BarefootService/GetPropertyBookingDateForAllXML</h2>
</blockquote>
		<cfreturn q>
    </cffunction>



    <!--- GetPropertyRatesXML --->
    <cffunction name="GetPropertyRatesXML" output="true" access="public" returntype="any" hint="">
		<cfset var q = ''>
		<Cfset this.mystart='#dateFormat( now(), 'yyyy-mm' )#-01'>
<Cfset this.mystart='#dateFormat(
	'#dateAdd(
		'd',
		-90,
		this.mystart
	)#',
	'yyyy-mm-dd'
)#'>
<Cfset this.myend='#dateFormat(
	'#dateAdd(
		'd',
		100,
		this.mystart
	)#',
	'yyyy-mm-dd'
)#'>
		<cfsetting requesttimeout="360">
			<cfsavecontent variable="this.barefootpost"><?xml version="1.0" encoding="utf-8"?>
			<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
			  <soap:Body>
			    <GetPropertyRatesXML xmlns="http://www.barefoot.com/Services/">
			      <username>#arguments.prc.barefoot.username#</username>
			      <password>#arguments.prc.barefoot.password#</password>
			      <barefootAccount>#arguments.prc.barefoot.barefootaccount#</barefootAccount>
			      <propertyId>#arguments.propid#</propertyId>
			      <date1>#this.mystart#</date1>
    			  <date2>#this.myend#</date2>
			    </GetPropertyRatesXML>
			  </soap:Body>
			</soap:Envelope>
			</cfsavecontent>
			<cfhttp url="#this.barefooturl#" method="post">
				<cfhttpparam name="Host" type="HEADER" value="agent.barefoot.com">
				<cfhttpparam name="Cache-Control" type="HEADER" value="no-cache">
				<cfhttpparam name="Content-Type" type="HEADER" value="text/xml; charset=utf-8">
				<cfhttpparam name="SOAPAction" type="HEADER" value='"#this.soapactionroot#GetPropertyRatesXML"'>
				<cfhttpparam name="" type="body" value="#this.barefootpost#">
			</cfhttp>
			<cfset q = xmlParse( #cfhttp.fileContent# )>

			<cfxml variable="q">#q.Envelope.Body.GetPropertyRatesXMLResponse.GetPropertyRatesXMLResult.XmlText#</cfxml>

		<cfreturn q>
    </cffunction>



    <!--- GetOwnerInfo --->
    <cffunction name="GetOwnerInfo" output="true" access="public" returntype="any" hint="">
		<cfset var q = ''>
		<cfsetting requesttimeout="360">
			<cfsavecontent variable="this.barefootpost"><?xml version="1.0" encoding="utf-8"?>
			<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
			  <soap:Body>
			    <GetOwnerInfo xmlns="http://www.barefoot.com/Services/">
			      <username>#arguments.prc.barefoot.username#</username>
			      <password>#arguments.prc.barefoot.password#</password>
			      <barefootAccount>#arguments.prc.barefoot.barefootaccount#</barefootAccount>
			    </GetOwnerInfo>
			  </soap:Body>
			</soap:Envelope>
			</cfsavecontent>
			<cfhttp url="#this.barefooturl#" method="post">
				<cfhttpparam name="Host" type="HEADER" value="agent.barefoot.com">
				<cfhttpparam name="Cache-Control" type="HEADER" value="no-cache">
				<cfhttpparam name="Content-Type" type="HEADER" value="text/xml; charset=utf-8">
				<cfhttpparam name="SOAPAction" type="HEADER" value='"#this.soapactionroot#GetOwnerInfo"'>
				<cfhttpparam name="" type="body" value="#this.barefootpost#">
			</cfhttp>
			<cfset q = xmlParse( #cfhttp.fileContent# )>

			<cfxml variable="q">#q.Envelope.Body.GetOwnerInfoResponse.GetOwnerInfoResult#</cfxml>

		<cfreturn q>
    </cffunction>



    <!--- GetTenantInfo --->
    <cffunction name="GetTenantInfo" output="true" access="public" returntype="any" hint="">
		<cfset var q = ''>
		<cfsetting requesttimeout="360">
			<cfsavecontent variable="this.barefootpost"><?xml version="1.0" encoding="utf-8"?>
			<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
			  <soap:Body>
			    <GetTenantInfo xmlns="http://www.barefoot.com/Services/">
			      <username>#arguments.prc.barefoot.username#</username>
			      <password>#arguments.prc.barefoot.password#</password>
			      <barefootAccount>#arguments.prc.barefoot.barefootaccount#</barefootAccount>
			    </GetTenantInfo>
			  </soap:Body>
			</soap:Envelope>
			</cfsavecontent>
			<cfhttp url="#this.barefooturl#" method="post">
				<cfhttpparam name="Host" type="HEADER" value="agent.barefoot.com">
				<cfhttpparam name="Cache-Control" type="HEADER" value="no-cache">
				<cfhttpparam name="Content-Type" type="HEADER" value="text/xml; charset=utf-8">
				<cfhttpparam name="SOAPAction" type="HEADER" value='"#this.soapactionroot#GetTenantInfo"'>
				<cfhttpparam name="" type="body" value="#this.barefootpost#">
			</cfhttp>
			<cfset q = xmlParse( #cfhttp.fileContent# )>

			<cfxml variable="q">#q.Envelope.Body.GetTenantInfoResponse.GetTenantInfoResult#</cfxml>

		<cfreturn q>
    </cffunction>

    


</cfcomponent>